import React from 'react';
import {StyleSheet} from 'react-native';
import Navigator from './src/navigation/Navigator';

export const MAIN_COLOR = '#18D6B8';
export const INACTIVE_GREY = '#939393';
export const DARK_GREY = '#454F55';
export const ACTIVE_GREEN = '#cdf4ea';
export const RU_PHONE_DIGITS = 11;
export const SMS_DIGITS = 6;
export const PASSWORD_DIGITS = 8;

const App = () => {
  return (
    <>
      <Navigator />
    </>
  );
};

const styles = StyleSheet.create({});

export default App;
