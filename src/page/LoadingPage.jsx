import React from 'react';
import {SafeAreaView, StatusBar, StyleSheet, Text, View} from 'react-native';
import SvgLogoWhite from '../components/ui/icons/SvgLogoWhite';

const LoadingPage = () => {
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        translucent={true}
        backgroundColor={'transparent'}
        barStyle="light-content"
      />
      <View style={styles.headerNameAppItem}>
        <SvgLogoWhite />
        <Text style={styles.headerNameAppText}>
          ЦЕНТР ЭПИЛЕПТОЛОГИИ И{'\n'}НЕВРОЛОГИИ.{'\n'}А.А.КАЗАРЯНА
        </Text>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: '#18D6B8',
  },
  headerNameAppItem: {
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    width: 334,
    height: 129,
    borderRadius: 3,
    borderWidth: 3,
    borderColor: 'transparent',
  },
  headerNameAppText: {
    fontSize: 20,
    color: 'white',
    textShadowRadius: 8,
  },
});

export default LoadingPage;
