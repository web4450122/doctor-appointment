import React, {useState, useEffect} from 'react';

import {ScrollView, StyleSheet, View} from 'react-native';
import NaviBar from '../components/ui/NaviBar';
import Footer from '../components/ui/Footer';
import AroundAddButton from '../components/ui/AroundAddButton';
import MedicineCard from '../components/MedicineCard';
import ModalDeleteAlert from '../components/modalWindow/ModalDeleteAlert';
import ModalEditor from '../components/modalWindow/ModalEditor';
import ModalAddSeizures from '../components/modalWindow/SeizuresPage/ModalAddSeizures';
import {useUpdateDataSupplemented} from '../hooks/useRequestBuilder';
import {getMinutesText} from '../assets/pluralization/Pluralization';

const SeizuresPage = () => {
  const [visibleModalEditer, setVisibleModalEditer] = useState(false);
  const [visibleAlertModalDeleteData, setVisibleAlertModalDeleteData] =
    useState(false);
  const [visibleModalSeizures, setVisibleModalSeizures] = useState(false);
  const [currentCardId, setCurrentCardId] = useState('');

  const [seizuresData, setSeizuresData] = useState([]);

  const [refreshData, setRefreshData] = useState(false); // Стейт для обновления данных в момент создания новой карточки
  const [deleteCard, setDeleteCard] = useState(false); // Стейт для обновления данных после удаление карочки
  const [editorCardMode, setEditorCardMode] = useState(false);

  const updateSeizurestData = () => {
    setRefreshData(!refreshData); // Изменяем стейт, чтобы вызвать перерендер
    setVisibleModalSeizures(!visibleModalSeizures);
    setVisibleModalEditer(false);
  };
  const updateCardAfterDelete = () => {
    setDeleteCard(!deleteCard);
  };

  function closeModalWindows() {
    setVisibleModalEditer(false);
    setVisibleAlertModalDeleteData(!visibleAlertModalDeleteData);
  }

  const updateData = useUpdateDataSupplemented();

  useEffect(() => {
    updateData(
      'Seizures',
      {
        id: 'id',
        seizure: 'seizure',
        currentDate: 'currentDate',
        currentTime: 'currentTime',
        continueMinutes: 'continueMinutes',
        sleepState: 'sleepState',
        comment: 'comment',
      },
      setSeizuresData,
    );
  }, [refreshData, deleteCard]);

  return (
    <View style={styles.container}>
      <NaviBar namePage={'ПРИСТУПЫ'} />
      <View style={styles.insulatingItem}>
        <ScrollView
          showsVerticalScrollIndicator={true}
          contentContainerStyle={{...styles.scrollView, paddingBottom: 80}}>
          {seizuresData.map((item, index) => (
            <MedicineCard
              key={index}
              header={item.seizure}
              visibleDoseIndicator={false}
              leftTopLabel={'Дата и время'}
              leftBottomLabel={item.currentDate + ' ' + item.currentTime}
              rightTopLabel={'Продолжительность'}
              rightBottomLabel={
                item.continueMinutes +
                ' ' +
                getMinutesText(item.continueMinutes)
              }
              comment={'Примечания'}
              description={item.comment}
              onEditerButton={() => {
                setVisibleModalEditer(!visibleModalEditer);
                setCurrentCardId(item.id);
              }}
            />
          ))}
          <ModalEditor
            visible={visibleModalEditer}
            closeMove={() => {
              setCurrentCardId('');
              setVisibleModalEditer(false);
              setEditorCardMode(false);
            }}
            editMove={() => {
              setEditorCardMode(true);
              setVisibleModalSeizures(!visibleModalSeizures);
            }}
            deleteMove={() =>
              setVisibleAlertModalDeleteData(!visibleAlertModalDeleteData)
            }
          />
          <ModalDeleteAlert
            visible={visibleAlertModalDeleteData}
            cancelMove={closeModalWindows}
            updateCardAfterDelete={updateCardAfterDelete}
            nameTable={'Seizures'}
            id={currentCardId}
          />
          <ModalAddSeizures
            reWriteStateInTable={editorCardMode}
            visible={visibleModalSeizures}
            visibleModalEditer={visibleModalEditer}
            closeMove={() => {
              setVisibleModalSeizures(!visibleModalSeizures);
              setVisibleModalEditer(false);
              setCurrentCardId('');
              setEditorCardMode(false);
            }}
            current_id={currentCardId}
            updateSeizurestData={updateSeizurestData} // Передача функции обновления данных внутрь модального окна создания карточки
          />
        </ScrollView>
        <AroundAddButton
          onPress={() => {
            setVisibleModalSeizures(!visibleModalSeizures);
          }}
        />
      </View>
      <Footer />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
  insulatingItem: {
    flex: 1,
    alignItems: 'center',
    width: '100%',
    height: '100%',
    marginBottom: '20%',
  },
  scrollView: {
    backgroundColor: 'transparent',
    alignItems: 'center',
    marginHorizontal: -10,
    minWidth: '100%',
    flexGrow: 1,
  },
  editButton: {
    color: 'white',
    fontSize: 30,
    fontWeight: 'bold',
    marginLeft: 8,
  },
});
export default SeizuresPage;
