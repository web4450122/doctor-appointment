import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  Image,
} from 'react-native';
import HorizontalSeparator from '../components/ui/HorizontalSeparator';
import LogInForm from '../components/authorization/LogInForm';

const LoginPage = () => {
  return (
    <LinearGradient colors={['#3C3C43', '#18D6B8']} style={styles.container}>
      <SafeAreaView style={styles.container}>
        <StatusBar translucent={false} />
        <View style={styles.headerNameAppItem}>
          <Image
            source={require('../assets/image/logo.png')}
            style={styles.logoImg}
            resizeMode="contain"
          />
          <HorizontalSeparator Styles={styles.separator} />
          <Text style={styles.headerNameAppText}>
            ЦЕНТР МРТ И НЕВРОЛОГИИИ ИМЕНИ{'\n'} Г.С. ХАНЗО
          </Text>
        </View>
        <LogInForm />
      </SafeAreaView>
    </LinearGradient>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
  headerNameAppItem: {
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    width: 334,
    height: 129,
    borderRadius: 3,
    borderWidth: 3,
    borderColor: 'transparent',
    marginTop: 80,
  },
  logoImg: {
    width: 90,
    height: 190,
  },
  headerNameAppText: {
    fontSize: 14,
    paddingLeft: 18,
    color: 'white',
    width: 234.77,
    height: 51.46,
    textShadowColor: '#000000',
    textShadowOffset: {
      width: -2,
      height: 5,
    },
    textShadowRadius: 8,
  },
  separator: {
    height: 0,
    width: 0,
    backgroundColor: 'transparent',
  },
});

export default LoginPage;
