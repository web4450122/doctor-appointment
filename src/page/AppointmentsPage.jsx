import React, {useState, useEffect} from 'react';
import {ScrollView, StyleSheet, View} from 'react-native';
import NaviBar from '../components/ui/NaviBar';
import Footer from '../components/ui/Footer';
import AroundAddButton from '../components/ui/AroundAddButton';
import MedicineCard from '../components/MedicineCard';
import ModalDeleteAlert from '../components/modalWindow/ModalDeleteAlert';
import ModalEditor from '../components/modalWindow/ModalEditor';
import ModalAddApointment from '../components/modalWindow/appointmentPage/ModalAddApointment';
import {useUpdateDataSupplemented} from '../hooks/useRequestBuilder';

const AppointmentsPage = () => {
  const [visibleModalEditer, setVisibleModalEditer] = useState(false);
  const [visibleAlertModalDeleteData, setVisibleAlertModalDeleteData] =
    useState(false);
  const [visibleModalAddApointment, setVisibleModalAddApointment] =
    useState(false);

  const [currentCardId, setCurrentCardId] = useState('');
  const [appointmentData, setAppointmentData] = useState([]);

  const [refreshData, setRefreshData] = useState(false); // Стейт для обновления данных в момент создания новой карточки
  const [deleteCard, setDeleteCard] = useState(false); // Стейт для обновления данных после удаление карочки
  const [editorCardMode, setEditorCardMode] = useState(false);

  const updateAppointmentData = () => {
    setRefreshData(!refreshData); // Изменяем стейт, чтобы вызвать перерендер
    setVisibleModalAddApointment(!visibleModalAddApointment);
    setVisibleModalEditer(false);
  };
  const updateCardAfterDelete = () => {
    setDeleteCard(!deleteCard);
  };

  function closeModalWindows() {
    setVisibleModalEditer(false);
    setVisibleAlertModalDeleteData(!visibleAlertModalDeleteData);
  }

  const updateData = useUpdateDataSupplemented();

  useEffect(() => {
    updateData(
      'Appointments',
      {
        id: 'id',
        service: 'service',
        currentDate: 'currentDate',
        currentTime: 'currentTime',
        address: 'address',
        remember: 'remember',
      },
      setAppointmentData,
    );
  }, [refreshData, deleteCard]);

  return (
    <View style={styles.container}>
      <NaviBar namePage={'ЗАПИСЬ В КЛИНИКУ'} />
      <View style={styles.insulatingItem}>
        <ScrollView
          showsVerticalScrollIndicator={true}
          contentContainerStyle={{...styles.scrollView, paddingBottom: 80}}>
          {appointmentData.map((item, index) => (
            <MedicineCard
              key={index}
              header={item.service}
              visibleDoseIndicator={false}
              leftTopLabel={'Дата и время'}
              leftBottomLabel={item.currentDate + '  ' + item.currentTime}
              rightTopLabel={'Адрес'}
              rightBottomLabel={item.address}
              description={item.remember}
              onEditerButton={() => {
                setVisibleModalEditer(!visibleModalEditer);
                setCurrentCardId(item.id);
              }}
            />
          ))}

          <ModalEditor
            visible={visibleModalEditer}
            closeMove={() => {
              setCurrentCardId('');
              setEditorCardMode(false);
              setVisibleModalEditer(false);
            }}
            editMove={() => {
              setEditorCardMode(true);
              setVisibleModalAddApointment(!visibleModalAddApointment);
            }}
            deleteMove={() =>
              setVisibleAlertModalDeleteData(!visibleAlertModalDeleteData)
            }
          />
          <ModalDeleteAlert
            visible={visibleAlertModalDeleteData}
            cancelMove={closeModalWindows}
            updateCardAfterDelete={updateCardAfterDelete}
            nameTable={'Appointments'}
            id={currentCardId}
          />
          <ModalAddApointment
            reWriteStateInTable={editorCardMode}
            visible={visibleModalAddApointment}
            visibleModalEditer={visibleModalEditer}
            closeMove={() => {
              setVisibleModalAddApointment(!visibleModalAddApointment);
              setVisibleModalEditer(false);
              setCurrentCardId('');
              setEditorCardMode(false);
            }}
            current_id={currentCardId}
            updateAppointmentData={updateAppointmentData} // Передача функции обновления данных внутрь модального окна создания карточки
          />
        </ScrollView>
        <AroundAddButton
          onPress={() => {
            setVisibleModalAddApointment(!visibleModalAddApointment);
          }}
        />
      </View>
      <Footer />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    width: '100%',
    height: '100%',
    // borderWidth: 2,
    // borderColor: 'red',
    // marginTop: 22,
  },
  insulatingItem: {
    flex: 1,
    alignItems: 'center',
    width: '100%',
    height: '100%',
    marginBottom: '20%',
    // borderWidth: 2,
    // borderColor: 'blue',
  },
  scrollView: {
    backgroundColor: 'transparent',
    alignItems: 'center',
    marginHorizontal: -10,
    minWidth: '100%',
    flexGrow: 1,
  },
  editButton: {
    color: 'white',
    fontSize: 30,
    fontWeight: 'bold',
    marginLeft: 8,
  },
});
export default AppointmentsPage;
