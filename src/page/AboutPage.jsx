import React from 'react';
import {
  StatusBar,
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
} from 'react-native';
import NaviBar from '../components/ui/NaviBar';
import Footer from '../components/ui/Footer';
import {MAIN_COLOR} from '../../App';

const AboutPage = () => {
  return (
    <>
      <View style={styles.container}>
        <StatusBar />
        <NaviBar namePage={'О ЦЕНТРЕ'} />
        <View style={styles.insulatingItem}>
          <ScrollView
            showsVerticalScrollIndicator={true}
            contentContainerStyle={styles.scrollView}>
            <View style={styles.titleNameItem}>
              <Image
                source={require('../assets/image/logo.png')}
                style={styles.logoImg}
                resizeMode="contain"
              />
              <Text style={styles.headerNameAppText}>
                ЦЕНТР МРТ И{'\n'}НЕВРОЛОГИИ. ИМ.{'\n'}Г.С. ХАНЗО
              </Text>
            </View>
            <View style={styles.corpImageItem}>
              <Image
                source={require('../assets/image/aboutCompany.png')}
                style={styles.mainImage}
                resizeMode="contain"
              />
            </View>
            <View style={styles.mainTextItem}>
              <Text style={styles.text}>
                <Text style={styles.text}>
                  <Text style={styles.boldText}>
                    Центр мрт и неврологии{'\t'}
                  </Text>
                  <Text style={[styles.greenText, {color: MAIN_COLOR}]}>
                    им. Г.С. Ханзо{'\t'}
                  </Text>
                </Text>
                создан в 2007 году с целью внедрения и развития современных
                доказательных подходов к диагностике и лечению неврологических
                заболеваний как у детей так и у взрослых. Практическая
                деятельность клиники включает комплексное обследование пациентов
                страдающих расстройствами сна, психогенными приступами,
                синкопальными состояниями и другими пароксизмальными
                нарушениями, а так же синдромом гиперактивности с дефицитом
                внимания {'\n\n'}Все наши врачи ориентированы на современные
                международные стандарты диагностики и лечения.
              </Text>
            </View>
          </ScrollView>
        </View>
        <Footer />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
  scrollView: {
    backgroundColor: 'transparent',
    alignItems: 'center',
  },
  insulatingItem: {
    flex: 1,
    alignItems: 'center',
    width: '100%',
    height: '100%',
    marginBottom: '20%',
  },
  logoImg: {
    width: 90,
    height: 190,
  },
  titleNameItem: {
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
    height: 90,
    width: '85%',
    paddingHorizontal: 1,
    marginTop: 14,
  },
  headerNameAppText: {
    fontSize: 16,
    paddingLeft: 18,
    color: 'black',
    textShadowRadius: 1,
  },
  corpImageItem: {
    width: 340,
    height: 210,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
  },
  mainTextItem: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: 340,
    marginTop: 15,
  },
  text: {
    color: 'black',
    textAlign: 'center',
    width: 320,
  },
  boldText: {
    fontWeight: 'bold',
    color: 'black',
  },
  greenText: {
    fontWeight: 'bold',
  },
  mainImage: {
    width: 320,
    height: 240,
  },
});

export default AboutPage;
