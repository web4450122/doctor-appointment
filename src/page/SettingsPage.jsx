import React, {useState, useEffect} from 'react';
import {useNavigation} from '@react-navigation/native';

import {StyleSheet, View} from 'react-native';
import NaviBar from '../components/ui/NaviBar';
import Footer from '../components/ui/Footer';
import Cbutton from '../components/ui/Cbutton';
import HorizontalSeparator from '../components/ui/HorizontalSeparator';
import SvgPerson from '../components/ui/icons/SvgSettings/SvgPerson';
import SvgArrowR from '../components/ui/icons/SvgUi/SvgArrowR';
import SvgLock from '../components/ui/icons/SvgSettings/SvgLock';
import {MAIN_COLOR} from '../../App';
import {useHelperTableOperations} from '../hooks/useRequestSqlHelper';
import {useSqlBuilder} from '../hooks/useRequestBuilder';

const SettingsPage = () => {
  const navigation = useNavigation();

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [surName, setSurname] = useState('');

  const {showTableContent} = useHelperTableOperations();
  const {updateData, deleteTable} = useSqlBuilder();

  useEffect(() => {
    updateData(
      [setFirstName, setLastName, setSurname],
      ['firstName', 'lastName', 'surName'],
      'ActiveUser',
    );
  }, []);

  const handleLogout = () => {
    deleteTable('ActiveUser');
  };

  return (
    <>
      <View style={styles.container}>
        <NaviBar namePage={'НАСТРОЙКИ'} />
        <View style={styles.insulatingItem}>
          <View style={styles.itemButtons}>
            <View>
              <Cbutton
                styleButton={styles.buttonPerson}
                styleText={styles.buttonTextBlack}
                name={`${firstName} ${lastName} ${surName}`}
                isShadow={'underline'}
                onPress={() => navigation.navigate('SettingsUserInfo')}
              />
              {/* <Cbutton
                styleButton={styles.buttonDebug}
                styleText={styles.buttonTextBlack}
                name={'показать данные'}
                isShadow={'underline'}
                onPress={() => showTableContent('ActiveUser')}
              /> */}
              <View style={styles.svgPositionL}>
                <SvgPerson />
              </View>
              <View style={styles.svgPositionR}>
                <SvgArrowR />
              </View>
            </View>
            <HorizontalSeparator Styles={styles.separator} />
            <View>
              <Cbutton
                styleButton={styles.buttonPassword}
                styleText={styles.buttonTextBlack}
                name={'Пароль'}
                isShadow={'underline'}
                onPress={() => navigation.navigate('SettingsPassword')}
              />
              <View style={styles.svgPositionL}>
                <SvgLock />
              </View>
              <View style={styles.svgPositionR}>
                <SvgArrowR />
              </View>
            </View>
            <HorizontalSeparator Styles={styles.separatorTwo} />
            <Cbutton
              styleButton={styles.buttonLogOut}
              colorButton={{backgroundColor: MAIN_COLOR}}
              name={'Выйти'}
              onPress={() => {
                navigation.navigate('LoginPage', {user: 'deleteAll'});
                handleLogout();
              }}
            />
          </View>
        </View>
        <Footer />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
  insulatingItem: {
    flex: 1,
    alignItems: 'center',
    width: '100%',
    height: '100%',
    marginBottom: '20%',
  },
  itemButtons: {
    width: '85%',
    marginTop: 25,
  },
  svgPositionL: {
    position: 'absolute',
    top: 16,
    left: 30,
  },
  svgPositionR: {
    position: 'absolute',
    top: 16,
    right: 20,
  },
  separator: {
    height: 2,
    backgroundColor: 'transparent',
  },
  separatorTwo: {
    height: 14,
    backgroundColor: 'transparent',
  },
  buttonTextBlack: {
    color: 'black',
    marginLeft: 60,
  },
  buttonPerson: {
    height: 50,
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 18,
    alignItems: 'flex-start',
  },
  buttonDebug: {
    height: 50,
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 18,
    alignItems: 'flex-start',
  },
  buttonPassword: {
    height: 50,
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 18,
    alignItems: 'flex-start',
  },
  buttonLogOut: {
    height: 50,
    width: '100%',
    borderRadius: 25,
  },
});

export default SettingsPage;
