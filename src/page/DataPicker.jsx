import React, {useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {Calendar, LocaleConfig} from 'react-native-calendars';
import NaviBar from '../components/ui/NaviBar';
import Footer from '../components/ui/Footer';

const DataPicker = () => {
  const [selected, setSelected] = useState('');
  return (
    <View style={styles.container}>
      <NaviBar namePage={'ВЫБОР ДАТЫ'} />
      <View style={styles.insulatingItem}>
        <View style={styles.ItemCalender}>
          <Calendar
            style={{
              height: 400,
              width: 300,
            }}
            theme={{
              backgroundColor: 'transparent',
              calendarBackground: 'transparent',
              textSectionTitleColor: '#b6c1cd',
              selectedDayBackgroundColor: '#18D6B8',
              selectedDayTextColor: '#ffffff',
              todayTextColor: '#00adf5',
              dayTextColor: '#2d4150',
              textDisabledColor: '#d9e3f0',
              dotColor: '#00adf5',
              selectedDotColor: '#ffffff',
              arrowColor: '#18D6B8',
              disabledArrowColor: '#d9e3f0',
              monthTextColor: 'black',
              indicatorColor: 'blue',
              textDayFontFamily: 'monospace',
              textMonthFontFamily: 'monospace',
              textDayHeaderFontFamily: 'monospace',
              textDayFontSize: 16,
              textMonthFontSize: 16,
              textDayHeaderFontSize: 16,
            }}
            current={selected}
            onDayPress={day => {
              setSelected(day.dateString);
            }}
            markedDates={{
              [selected]: {
                selected: true,
                disableTouchEvent: true,
                selectedDotColor: 'orange',
                selectedColor: '#18D6B8',
              },
            }}
          />
        </View>
      </View>
      <Footer />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    // borderWidth: 2,
    // borderColor: 'red',
    // marginTop: 22,
  },
  insulatingItem: {
    flex: 1,
    alignItems: 'center',
    width: '100%',
    height: '100%',
    marginBottom: '20%',
    // borderWidth: 2,
    // borderColor: 'blue',
  },
  ItemCalender: {
    height: '100%',
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default DataPicker;
