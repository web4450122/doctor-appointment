import React, {useState} from 'react';
import {
  StatusBar,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  TextInput,
} from 'react-native';
import NaviBar from '../components/ui/NaviBar';
import Footer from '../components/ui/Footer';
import SvgSearch from '../components/ui/icons/SvgChatPage/SvgSearch';
import HorizontalSeparator from '../components/ui/HorizontalSeparator';
import {useNavigation} from '@react-navigation/native';

const MessagesPage = () => {
  const navigation = useNavigation();
  let LayoutInspector = false; // для дебага

  //
  //
  // ВЕСЬ КОД С ЧАТОМ НА ДАННЫЙ МОМЕНТ ЗАГЛУШКА И НЕ БОЛЕЕ
  //
  //

  const [inputText, SetInputText] = useState('');
  let inputData = [
    {
      id: 'f322',
      messages: [
        {
          id: 1,
          message: 'привет',
        },
        {
          id: 2,
          message: 'как дела',
        },
        {
          id: 3,
          message: 'как ты',
        },
      ],
      chatInfo: {
        name: 'АНТОНОВ АЛЕКСАНДР',
        surname: 'АЛЕКСЕЕВИЧ',
        date: '10.12.2023',
        missedMessageCount: 2,
      },
    },
    {
      id: 'f1488',
      messages: [
        {
          id: 1,
          message: 'привет',
        },
        {
          id: 2,
          message: 'как дела',
        },
        {
          id: 3,
          message: 'доктор',
        },
      ],
      chatInfo: {
        name: 'ГЛЕБ БЕЛЫЙ',
        surname: 'АНАТОЛЬЕВИЧ',
        date: '10.12.2019',
        missedMessageCount: 1,
      },
    },
  ];

  let listMessages = inputData.map(chat => {
    const lastMessage = chat.messages[chat.messages.length - 1].message;

    return {
      name: chat.chatInfo.name,
      surname: chat.chatInfo.surname,
      lastMessage: lastMessage,
      date: chat.chatInfo.date,
      missedMessageCount: chat.chatInfo.missedMessageCount,
    };
  });

  function getInitials(name, surname) {
    return (
      name
        .split(' ')
        .map(word => word.charAt(0))
        .join('') +
      surname
        .split(' ')
        .map(word => word.charAt(0))
        .join('')
    );
  }

  return (
    <>
      <View style={styles.container}>
        <StatusBar />
        <NaviBar namePage={'ЧАТЫ'} />
        <View
          style={{
            width: '85%',
            height: 50,
            marginTop: 14,
            marginBottom: 10,
          }}>
          <View style={styles.svgButtonsInput}>
            <SvgSearch />
          </View>
          <TextInput
            style={styles.input}
            placeholder="Поиск"
            value={inputText}
            onChangeText={text => SetInputText(text)}
            keyboardType="default"
          />
        </View>
        <View
          style={[
            styles.insulatingItem,
            LayoutInspector ? {borderWidth: 2, borderColor: 'blue'} : null,
          ]}>
          <ScrollView
            showsVerticalScrollIndicator={true}
            contentContainerStyle={styles.scrollView}>
            {listMessages.map((item, index) => (
              <React.Fragment key={index}>
                <TouchableOpacity
                  onPress={() => {
                    navigation.navigate('MessagesObjectPage', {
                      chatName: `${item.name} ${item.surname}`,
                    });
                  }}
                  style={{
                    borderWidth: LayoutInspector ? 2 : null,
                    width: '90%',
                    marginBottom: 2,
                  }}>
                  <View
                    style={[
                      styles.chatItem,
                      LayoutInspector
                        ? {borderWidth: 2, borderColor: 'pink'}
                        : null,
                    ]}>
                    <View style={styles.avatar}>
                      <Text style={{textAlign: 'center'}}>
                        {getInitials(item.name, item.surname)}
                      </Text>
                    </View>
                    <View
                      style={[
                        styles.nameUserAndSomeTextChatItem,
                        LayoutInspector && {borderWidth: 2},
                      ]}>
                      <Text
                        style={{
                          fontSize: 16,
                          color: 'black',
                          fontWeight: 'bold',
                        }}>
                        {item.name}
                        {'\n'}
                        {item.surname}
                      </Text>
                      <Text>{item.lastMessage}</Text>
                    </View>
                    <View
                      style={[
                        styles.dateLastMessageItem,
                        LayoutInspector && {borderWidth: 2},
                      ]}>
                      <Text style={{fontSize: 11}}>{item.date}</Text>
                    </View>
                    <View
                      style={[
                        styles.counterSumMessagesItem,
                        {
                          display:
                            item.missedMessageCount !== 0 ? 'flex' : 'none',
                        },
                      ]}>
                      <Text style={{textAlign: 'center', color: 'white'}}>
                        {item.missedMessageCount}
                      </Text>
                    </View>
                  </View>
                  <HorizontalSeparator Styles={styles.separator} />
                </TouchableOpacity>
              </React.Fragment>
            ))}
          </ScrollView>
        </View>
        <Footer />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    // borderWidth: 2,
    // borderColor: 'red',
    // marginTop: 22,
  },
  input: {
    height: 45,
    width: '100%',
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 20,
    paddingHorizontal: 55,
  },
  svgButtonsInput: {
    position: 'absolute',
    top: 10,
    left: 15,
  },
  insulatingItem: {
    flex: 1,
    alignItems: 'center',
    width: '100%',
    height: '100%',
    marginBottom: '20%',
    // borderWidth: 2,
    // borderColor: 'blue',
  },
  chatItem: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
    height: 90,
    width: '100%',
    paddingHorizontal: 1,
    marginTop: 14,
  },
  scrollView: {
    backgroundColor: 'transparent',
    alignItems: 'center',
    marginHorizontal: -10,
    minWidth: '100%',
    flexGrow: 1,
  },
  avatar: {
    justifyContent: 'center',
    height: 50,
    width: 50,
    borderRadius: 25,
    backgroundColor: '#6A6A6D99',
  },
  nameUserAndSomeTextChatItem: {
    display: 'flex',
    justifyContent: 'flex-start',
    marginLeft: 12,
  },
  dateLastMessageItem: {
    display: 'flex',
    position: 'absolute',
    top: 10,
    right: 10,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    marginLeft: 12,
    // marginBottom: 40,
  },
  counterSumMessagesItem: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: 24,
    width: 24,
    borderRadius: 10,
    backgroundColor: '#18D6B8',
    position: 'absolute',
    right: 10,
    bottom: 10,
  },
  separator: {
    height: 1,
    width: '100%',
  },
});

export default MessagesPage;
