import React, {useState} from 'react';
import SvgLock from '../components/ui/icons/SvgSettings/SvgLock';

import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import NaviBar from '../components/ui/NaviBar';
import Footer from '../components/ui/Footer';
import Cbutton from '../components/ui/Cbutton';
import {MAIN_COLOR, DARK_GREY} from '../../App';

const SettingsPasswordPage = () => {
  const [secureTextEntry, setSecureTextEntry] = useState(true);
  const [oldPassword, setOldPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [repeatPassword, setRepeatPasswordPassword] = useState('');

  const showPassword = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  const sendNewPassword = () => {
    newPassword === repeatPassword
      ? console.log('пароль совпал')
      : console.log('пароль не совпал');
  };

  return (
    <>
      <View style={styles.container}>
        <NaviBar namePage={'НАСТРОЙКИ'} />
        <View style={styles.insulatingItem}>
          <View style={styles.headerTextItem}>
            <Text style={styles.headerText}>Смена пароля</Text>
          </View>
          <View style={styles.dataItem}>
            <Text style={styles.oldPassTest}>Старый пароль</Text>

            <TextInput
              style={styles.input}
              placeholderTextColor={DARK_GREY}
              placeholder="Пароль"
              value={oldPassword}
              onChangeText={text => setOldPassword(text)}
              secureTextEntry={secureTextEntry}
            />
            <TouchableOpacity
              style={styles.touchableOpacity}
              onPress={showPassword}>
              <SvgLock scale={0.7} />
            </TouchableOpacity>

            <Text style={styles.newPassText}>Новый пароль</Text>

            <TextInput
              style={styles.input}
              placeholderTextColor={DARK_GREY}
              placeholder="Новый пароль"
              value={newPassword}
              onChangeText={text => setNewPassword(text)}
              secureTextEntry={secureTextEntry}
            />
            <TouchableOpacity
              style={styles.touchableOpacityTwo}
              onPress={showPassword}>
              <SvgLock scale={0.7} />
            </TouchableOpacity>

            <TextInput
              style={styles.input}
              placeholderTextColor={DARK_GREY}
              placeholder="Повторите пароль"
              value={repeatPassword}
              onChangeText={text => setRepeatPasswordPassword(text)}
              secureTextEntry={secureTextEntry}
            />
          </View>
          <Cbutton
            styleButton={styles.buttonSend}
            colorButton={{backgroundColor: MAIN_COLOR}}
            name={'Отправить'}
            onPress={sendNewPassword}
            isDisabled={!(newPassword.length > 0 && repeatPassword.length > 0)}
          />
        </View>
        <Footer />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
  insulatingItem: {
    flex: 1,
    alignItems: 'center',
    width: '100%',
    height: '100%',
    marginBottom: '20%',
    // borderWidth: 2,
    // borderColor: 'blue',
  },
  headerTextItem: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    width: '85%',
    // borderWidth: 2,
  },
  headerText: {
    fontSize: 18,
    color: 'black',
  },
  oldPassTest: {
    marginBottom: 4,
    color: 'black',
  },
  newPassText: {
    margin: 4,
    color: 'black',
  },
  dataItem: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    width: '85%',
    // borderWidth: 2,
  },
  input: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    height: 36,
    width: '80%',
    backgroundColor: 'white',
    borderRadius: 10,
    borderWidth: 1,
    margin: 3,
    paddingVertical: 3,
    paddingHorizontal: 10,
    // marginTop: 0,
  },
  touchableOpacity: {
    position: 'absolute',
    top: 35,
    right: 50,
  },
  touchableOpacityTwo: {
    position: 'absolute',
    top: 105,
    right: 50,
  },
  buttonSend: {
    height: 36,
    width: '40%',
    marginTop: 40,
    borderRadius: 15,
  },
});

export default SettingsPasswordPage;
