import React, {useState} from 'react';
import {
  StatusBar,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Image,
} from 'react-native';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import NaviBar from '../components/ui/NaviBar';
import SvgSend from '../components/ui/icons/SvgChatPage/SvgSend';
import SvgAttach from '../components/ui/icons/SvgChatPage/SvgAttach';
import MessageItem from '../components/messagePageComponents/MessageItem';
import CiconButtons from '../components/ui/CiconButtons';

const MessagesObjectPage = ({route}) => {
  const {chatName} = route.params;
  const [inputText, SetInputText] = useState('');

  let LayoutInspector = false; // для дебага

  const [messages, setMessages] = useState([
    {
      text: 'Доктор мне плохо',
      sender: 'user1',
    },
    {text: 'Спасибо доктор', img: '', sender: 'user2'},
  ]);

  const [selectImage, setSelectImage] = useState('');

  const ImagePicker = () => {
    let options = {
      storageOptions: {
        path: 'image',
      },
    };

    launchImageLibrary(options, response => {
      if (response.assets && response.assets.length > 0) {
        setSelectImage(response.assets[0].uri);
        console.log(response.assets[0].uri);
      } else {
        console.log('изображение не выбрано');
      }
    });
  };

  return (
    <>
      <View style={styles.container}>
        <StatusBar />
        <NaviBar namePage={chatName} />
        <View
          style={[
            styles.insulatingItem,
            LayoutInspector ? {borderWidth: 2, borderColor: 'blue'} : null,
          ]}>
          <ScrollView
            showsVerticalScrollIndicator={true}
            contentContainerStyle={styles.scrollView}>
            {messages.map((item, index) => (
              <MessageItem key={index} message={item.text} img={item.img} />
            ))}
          </ScrollView>
        </View>
        <View style={styles.footer}>
          <View style={styles.inputItem}>
            <View style={styles.svgButtonAttach}>
              <CiconButtons
                icon={<SvgAttach />}
                onPress={() => {
                  ImagePicker();
                }}
              />
            </View>
            <TextInput
              style={styles.input}
              placeholder="Написать"
              value={inputText}
              onChangeText={text => SetInputText(text)}
              keyboardType="default"
            />
            <View style={styles.svgButtonSend}>
              <CiconButtons
                icon={<SvgSend />}
                onPress={() => {
                  if (inputText.length > 0 || selectImage.length > 0) {
                    if (inputText.length > 0 && selectImage.length === 0) {
                      const newMessage = {text: inputText};
                      SetInputText('');
                      setMessages([...messages, newMessage]);
                    } else if (
                      selectImage.length > 0 &&
                      inputText.length === 0
                    ) {
                      const newImg = {img: selectImage};
                      setSelectImage('');
                      setMessages([...messages, newImg]);
                    } else {
                      const newImgAndMessage = {
                        text: inputText,
                        img: selectImage,
                      };
                      SetInputText('');
                      setSelectImage('');
                      setMessages([...messages, newImgAndMessage]);
                    }
                  }
                }}
              />
            </View>
          </View>
          {selectImage ? (
            <TouchableOpacity
              style={styles.areaAttachImg}
              onPress={() => {
                setSelectImage('');
              }}>
              <Image
                source={{uri: selectImage}}
                style={{
                  width: 60,
                  height: 60,
                  resizeMode: 'cover',
                  borderRadius: 30,
                }}
              />
              <Text style={styles.closeButton}>X</Text>
            </TouchableOpacity>
          ) : null}
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    // borderWidth: 2,
    // borderColor: 'red',
    // marginTop: 22,
  },
  insulatingItem: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    width: '100%',
    height: '100%',
    marginBottom: '0%',
    // borderWidth: 2,
    // borderColor: 'blue',
  },
  scrollView: {
    backgroundColor: 'transparent',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    marginHorizontal: 0,
    minWidth: '100%',
    flexGrow: 1,
  },
  footer: {
    height: '17%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    backgroundColor: 'white',
    // borderWidth: 2,
  },
  inputItem: {
    width: '90%',
    height: 50,
    marginBottom: 15, // 60
    // borderWidth: 2,
  },
  input: {
    position: 'absolute',
    right: 3,
    height: 45,
    width: '90%',
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 20,
    paddingHorizontal: 15,
  },
  svgButtonAttach: {
    position: 'absolute',
    top: 10,
    left: 2,
  },
  svgButtonSend: {
    position: 'absolute',
    top: 10,
    right: 15,
  },
  areaAttachImg: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'row',
    width: '100%',
    // borderWidth: 2,
    paddingLeft: 60,
  },
  closeButton: {
    color: 'black',
    fontSize: 10,
    fontWeight: 'bold',
    marginRight: 30,
  },
});

export default MessagesObjectPage;
