import React from 'react';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  View,
  ScrollView,
} from 'react-native';
import NaviBar from '../components/ui/NaviBar';
import HealthCounter from '../components/mainPageWidgets/HealthCounter';
import MiniCalendar from '../components/mainPageWidgets/MiniCalendar';
import MainPageAppointmentCard from '../components/mainPageWidgets/MainPageAppointmentCard';
import MainPageAttackCard from '../components/mainPageWidgets/MainPageAttackCard';
import MainPageReminderMedicaments from '../components/mainPageWidgets/MainPageReminderMedicaments';
import Footer from '../components/ui/Footer';

const MainPage = ({}) => {
  return (
    <>
      <SafeAreaView style={styles.container}>
        <StatusBar />
        <NaviBar />
        <View style={styles.insulatingItem}>
          <ScrollView contentContainerStyle={styles.scrollView}>
            <HealthCounter />
            <MiniCalendar />
            <MainPageAppointmentCard />
            <MainPageReminderMedicaments />
            <MainPageAttackCard />
          </ScrollView>
        </View>
        <Footer />
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
  insulatingItem: {
    flex: 1,
    alignItems: 'center',
    width: '100%',
    height: '100%',
    marginBottom: '20%',
  },
  scrollView: {
    backgroundColor: 'transparent',
    alignItems: 'center',
    marginHorizontal: -20,
    paddingBottom: 20,
    flexGrow: 1,
  },
});

export default MainPage;
