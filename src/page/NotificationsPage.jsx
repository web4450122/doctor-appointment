import React from 'react';

import {StyleSheet, View} from 'react-native';
import NaviBar from '../components/ui/NaviBar';
import Footer from '../components/ui/Footer';
import NotificationItem from '../components/notifications/NotificationItem';

const NotificationsPage = () => {
  return (
    <View style={styles.container}>
      <NaviBar namePage={'УВЕДОМЛЕНИЯ'} />
      <View style={styles.insulatingItem}>
        <NotificationItem />
      </View>
      <Footer />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
  insulatingItem: {
    flex: 1,
    alignItems: 'center',
    width: '100%',
    height: '100%',
    marginBottom: '20%',
  },
});

export default NotificationsPage;
