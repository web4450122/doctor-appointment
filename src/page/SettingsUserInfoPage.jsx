import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import NaviBar from '../components/ui/NaviBar';
import Footer from '../components/ui/Footer';
import {useUpdateData} from '../hooks/useRequestBuilder';
import {DARK_GREY} from '../../App';

const SettingsInfoUserPage = () => {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [surName, setSurname] = useState('');

  const updateData = useUpdateData();

  useEffect(() => {
    updateData(
      [setFirstName, setLastName, setSurname],
      ['firstName', 'lastName', 'surName'],
      'ActiveUser',
    );
  }, []);

  return (
    <>
      <View style={styles.container}>
        <NaviBar namePage={'НАСТРОЙКИ'} />
        <View style={styles.insulatingItem}>
          <View style={styles.aboutUserItem}>
            <Text style={[styles.textNameField, {color: DARK_GREY}]}>
              Фамилия
            </Text>
            <View style={styles.field}>
              <Text style={[styles.textData, {color: DARK_GREY}]}>
                {lastName}
              </Text>
            </View>
            <Text style={[styles.textNameField, {color: DARK_GREY}]}>Имя</Text>
            <View style={styles.field}>
              <Text style={[styles.textData, {color: DARK_GREY}]}>
                {firstName}
              </Text>
            </View>
            <Text style={[styles.textNameField, {color: DARK_GREY}]}>
              Отчество
            </Text>
            <View style={styles.field}>
              <Text style={[styles.textData, {color: DARK_GREY}]}>
                {surName}
              </Text>
            </View>
            <Text style={[styles.textNameField, {color: DARK_GREY}]}>
              Дата рождения
            </Text>
            <View style={styles.dateBornField}></View>
          </View>
        </View>
        <Footer />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
  insulatingItem: {
    flex: 1,
    alignItems: 'center',
    width: '100%',
    height: '100%',
    marginBottom: '20%',
    // borderWidth: 2,
    // borderColor: 'blue',
  },
  aboutUserItem: {
    marginTop: 20,
    width: '85%',
    // borderWidth: 2,
  },
  field: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    height: 30,
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 10,
    margin: 2,
  },
  dateBornField: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    height: 30,
    width: '40%',
    backgroundColor: 'white',
    borderRadius: 10,
    margin: 2,
  },
  textNameField: {
    marginLeft: 10,
  },
  textData: {
    marginLeft: 10,
  },
});

export default SettingsInfoUserPage;
