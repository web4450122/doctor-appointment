import React, {useState, useEffect} from 'react';
import {ScrollView, StyleSheet, View} from 'react-native';
import NaviBar from '../components/ui/NaviBar';
import Footer from '../components/ui/Footer';
import AroundAddButton from '../components/ui/AroundAddButton';
import MedicineCard from '../components/MedicineCard';
import ModalDeleteAlert from '../components/modalWindow/ModalDeleteAlert';
import ModalEditor from '../components/modalWindow/ModalEditor';
import ModalAddMedicament from '../components/modalWindow/MedicinePage/ModalAddMedicament';
import {useUpdateDataSupplemented} from '../hooks/useRequestBuilder';

const MedicinePage = () => {
  const [visibleModalEditer, setVisibleModalEditer] = useState(false);
  const [visibleAlertModalDeleteData, setVisibleAlertModalDeleteData] =
    useState(false);
  const [visibleModalMedicament, setVisibleModalMedicament] = useState(false);
  const [currentCardId, setCurrentCardId] = useState('');

  const [medData, setMedData] = useState([]);

  const [refreshData, setRefreshData] = useState(false); // Стейт для обновления данных в момент создания новой карточки
  const [deleteCard, setDeleteCard] = useState(false); // Стейт для обновления данных после удаление карочки
  const [editorCardMode, setEditorCardMode] = useState(false); // TODO написать описание работы, режима редактирования

  const updateMedicamentsData = () => {
    setRefreshData(!refreshData); // Изменяем стейт, чтобы вызвать перерендер
    setVisibleModalMedicament(!visibleModalMedicament); // закрываем модальное окно добавления
    setVisibleModalEditer(false);
  };
  const updateCardAfterDelete = () => {
    setDeleteCard(!deleteCard);
  };

  function closeModalWindows() {
    setVisibleModalEditer(false);
    setVisibleAlertModalDeleteData(!visibleAlertModalDeleteData);
  }

  const updateData = useUpdateDataSupplemented();

  useEffect(() => {
    updateData(
      'Medicaments',
      {
        id: 'id',
        preparat: 'preparat',
        dose: 'dose',
        rotationday: 'rotationDay',
        startUse: 'startUse',
        endUse: 'endUse',
        reminderOne: 'reminderOne',
        reminderTwo: 'reminderTwo',
        reminderThree: 'reminderThree',
        comment: 'comment',
      },
      setMedData,
    );
  }, [refreshData, deleteCard]);

  return (
    <View style={styles.container}>
      <NaviBar namePage={'ПРИЕМ ЛЕКАРСТВ'} />
      <View style={styles.insulatingItem}>
        <ScrollView
          showsVerticalScrollIndicator={true}
          contentContainerStyle={{...styles.scrollView, paddingBottom: 80}}>
          {medData.map((item, index) => (
            <MedicineCard
              key={index}
              header={item.preparat}
              dose={item.dose}
              visibleDoseIndicator={true}
              leftTopLabel={'Переодичность'}
              leftBottomLabel={'3 раза в день'}
              rightTopLabel={'Продолжительность'}
              rightBottomLabel={item.startUse + ' - ' + item.endUse}
              comment={'Примечания'}
              description={item.comment}
              onEditerButton={() => {
                setVisibleModalEditer(!visibleModalEditer);
                setCurrentCardId(item.id);
              }}
            />
          ))}

          <ModalEditor
            visible={visibleModalEditer}
            closeMove={() => {
              setCurrentCardId('');
              setVisibleModalEditer(false);
              setEditorCardMode(false);
            }}
            editMove={() => {
              setEditorCardMode(true);
              setVisibleModalMedicament(!visibleModalMedicament);
            }}
            deleteMove={() =>
              setVisibleAlertModalDeleteData(!visibleAlertModalDeleteData)
            }
          />
          <ModalDeleteAlert
            visible={visibleAlertModalDeleteData}
            cancelMove={closeModalWindows}
            updateCardAfterDelete={updateCardAfterDelete}
            nameTable={'Medicaments'}
            id={currentCardId}
          />
          <ModalAddMedicament
            reWriteStateInTable={editorCardMode}
            visible={visibleModalMedicament}
            visibleModalEditer={visibleModalEditer}
            closeMove={() => {
              setVisibleModalMedicament(!visibleModalMedicament);
              setVisibleModalEditer(false);
              setCurrentCardId('');
              setEditorCardMode(false);
            }}
            current_id={currentCardId}
            updateMedicamentsData={updateMedicamentsData} // Передача функции обновления данных внутрь модального окна создания карточки
          />
        </ScrollView>
        <AroundAddButton
          onPress={() => {
            setVisibleModalMedicament(!visibleModalMedicament);
          }}
        />
      </View>
      <Footer />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
  insulatingItem: {
    flex: 1,
    alignItems: 'center',
    width: '100%',
    height: '100%',
    marginBottom: '20%',
  },
  scrollView: {
    backgroundColor: 'transparent',
    alignItems: 'center',
    marginHorizontal: -10,
    minWidth: '100%',
    flexGrow: 1,
  },
  editButton: {
    color: 'white',
    fontSize: 30,
    fontWeight: 'bold',
    marginLeft: 8,
  },
});
export default MedicinePage;
