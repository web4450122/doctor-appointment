/* eslint-disable prettier/prettier */
import React from 'react';
import {useNavigation} from '@react-navigation/native';
import DataPicker from '../../page/DataPicker';

const ScreenMessagesPage = () => {
  const navigation = useNavigation();
  return <DataPicker />;
};

export default ScreenMessagesPage;
