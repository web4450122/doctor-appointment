import React from 'react';
import {useNavigation} from '@react-navigation/native';
import LoginPage from '../../page/LoginPage';

const ScreenLoginPage = () => {
  const navigation = useNavigation();
  return <LoginPage />;
};

export default ScreenLoginPage;
