/* eslint-disable prettier/prettier */
import React from 'react';
import NotificationsPage from '../../page/NotificationsPage';

const ScreenNotificationsPage = () => {
  return <NotificationsPage />;
};

export default ScreenNotificationsPage;
