import React from 'react';
import {useNavigation} from '@react-navigation/native';
import MessagesObjectPage from '../../page/MessagesObjectPage';

const ScreenMessagesObjectPage = () => {
  const navigation = useNavigation();
  return <MessagesObjectPage />;
};

export default ScreenMessagesObjectPage;
