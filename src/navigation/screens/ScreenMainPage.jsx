/* eslint-disable prettier/prettier */
import React from 'react';
import {useNavigation} from '@react-navigation/native';
import MainPage from '../../page/MainPage';
import {useRoute} from '@react-navigation/native';

const ScreenMainPage = () => {
  const route = useRoute();
  const {name} = route.params;
  return <MainPage _name={name} />;
};

export default ScreenMainPage;
