/* eslint-disable prettier/prettier */
import * as React from 'react';
import SQLite from 'react-native-sqlite-storage';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  FlatList,
  Button,
} from 'react-native';
import {NavigationContainer, useNavigation} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Cbutton from '../../components/ui/Cbutton';

//
// ТЕСТОВЫЙ ФАЙЛ
//

function HomeScreen() {
  const navigation = useNavigation();

  // Создание или открытие базы данных
  const db = SQLite.openDatabase({name: 'myDatabase.db', location: 'default'});

  // Создание таблицы
  db.transaction(tx => {
    tx.executeSql(
      'CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, email TEXT)',
      [],
      (tx, result) => {
        console.log(result);
      },
      error => {
        console.log(error);
      },
    );
  });

  // Функция для вставки данных пользователя
  const insertUser = () => {
    db.transaction(tx => {
      tx.executeSql(
        'INSERT INTO users (name, email) VALUES ("John Doe", "john@example.com"), ("Milan", "test@test.pi")',
        [],
        (tx, result) => {
          console.log('Users inserted successfully');
        },
        error => {
          console.log('Error inserting users:', error);
        },
      );
    });
  };

  // Функция для удаления всех пользователей из таблицы
  const deleteAllUsers = () => {
    db.transaction(tx => {
      tx.executeSql(
        'DELETE FROM users',
        [],
        (tx, result) => {
          console.log('All users deleted successfully');
        },
        error => {
          console.log('Error deleting users:', error);
        },
      );
    });
  };

  // Функция для выборки всех пользователей
  const fetchUsers = () => {
    db.transaction(tx => {
      tx.executeSql(
        'SELECT * FROM users',
        [],
        (tx, result) => {
          const len = result.rows.length;
          for (let i = 0; i < len; i++) {
            const row = result.rows.item(i);
            console.log(
              `User ID: ${row.id}, Name: ${row.name}, Email: ${row.email}`,
            );
          }
        },
        error => {
          console.log('Error fetching users:', error);
        },
      );
    });
  };

  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <TouchableOpacity
        style={{borderWidth: 2}}
        onPress={() => navigation.navigate('AboutPage')}>
        <Text>Home Screen</Text>
      </TouchableOpacity>
      <Button title="Insert User" onPress={insertUser} />
      <Button title="Fetch Users" onPress={fetchUsers} />
      <Button title="Delete Users" onPress={deleteAllUsers} />
    </View>
  );
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//     width: '100%',
//     height: '100%',
//     borderWidth: 2,
//     borderColor: 'red',
//     // marginTop: 22,
//   },
//   insulatingItem: {
//     flex: 1,
//     alignItems: 'center',
//     width: '100%',
//     height: '100%',
//     marginBottom: '20%',
//     // borderWidth: 2,
//     // borderColor: 'blue',
//   },
// });
//
// styles.container
export default HomeScreen;
