/* eslint-disable prettier/prettier */
import React from 'react';
import SettingsPasswordPage from '../../page/SettingsPasswordPage';

const ScreenSettingsPassword = () => {
  return <SettingsPasswordPage />;
};

export default ScreenSettingsPassword;
