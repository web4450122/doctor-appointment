import React from 'react';
import {useNavigation} from '@react-navigation/native';
import MessagesPage from '../../page/MessagesPage';

const ScreenMessagesPage = () => {
  const navigation = useNavigation();
  return <MessagesPage />;
};

export default ScreenMessagesPage;
