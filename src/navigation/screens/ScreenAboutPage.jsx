import React from 'react';
import {useNavigation} from '@react-navigation/native';
import AboutPage from '../../page/AboutPage';

const ScreenAboutPage = () => {
  const navigation = useNavigation();
  return <AboutPage />;
};

export default ScreenAboutPage;
