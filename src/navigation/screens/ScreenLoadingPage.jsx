import React from 'react';
import {useNavigation} from '@react-navigation/native';
import LoadingPage from '../../page/LoadingPage';

const ScreenLoadingPage = () => {
  const navigation = useNavigation();
  return <LoadingPage />;
};

export default ScreenLoadingPage;
