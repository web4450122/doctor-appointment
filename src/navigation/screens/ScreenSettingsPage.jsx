/* eslint-disable prettier/prettier */
import React from 'react';
import {useNavigation} from '@react-navigation/native';
import SettingsPage from '../../page/SettingsPage';
import {useRoute} from '@react-navigation/native';

const ScreenSettingsPage = () => {
  const route = useRoute();
  const {name} = route.params;
  return <SettingsPage name={name} />;
};

export default ScreenSettingsPage;
