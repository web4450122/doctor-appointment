/* eslint-disable prettier/prettier */
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import React from 'react';
import ScreenLoadingPage from '../screens/ScreenLoadingPage';
import ScreenLoginPage from '../screens/ScreenLoginPage';
import ScreenMainPage from '../screens/ScreenMainPage';
import ScreenAboutPage from '../screens/ScreenAboutPage';
import DataPicker from '../../page/DataPicker';
import HomeScreen from '../screens/HomeScreen';
import ScreenMessagesPage from '../screens/ScreenMessagesPage';
import MessagesObjectPage from '../../page/MessagesObjectPage';
import ScreenSettingsPage from '../screens/ScreenSettingsPage';
import ScreenSettingsInfoUserPage from '../screens/ScreenSettingsUserInfoPage';
import ScreenSettingsPassword from '../screens/ScreenSettingsPassword';
import ScreenMedicinePage from '../screens/ScreenMedicinePage';
import ScreenSeizuresPage from '../screens/ScreenSeizuresPage';
import ScreenAppointmentsPage from '../screens/ScreenAppointmentsPage';
import ScreenNotificationsPage from '../screens/ScreenNotificationsPage';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const MainScreen = () => (
  <Tab.Navigator
    tabBar={() => null}
    screenOptions={{
      headerShown: false,
      tabBarVisible: false,
    }}>
    <Tab.Screen name="LoginPage" component={ScreenLoginPage} />
    <Tab.Screen name="LoadingPage" component={ScreenLoadingPage} />
    <Tab.Screen name="MessagesPage" component={ScreenMessagesPage} />
    <Tab.Screen
      name="MainPage"
      component={ScreenMainPage}
      initialParams={{name: ''}}
    />
    <Tab.Screen name="AboutPage" component={ScreenAboutPage} />
    <Tab.Screen name="DataPicker" component={DataPicker} />
    <Tab.Screen
      name="Settings"
      component={ScreenSettingsPage}
      initialParams={{name: 'tes'}}
    />
    <Tab.Screen name="MedicinePage" component={ScreenMedicinePage} />
    <Tab.Screen name="SeizuresPage" component={ScreenSeizuresPage} />
    <Tab.Screen name="AppointmentsPage" component={ScreenAppointmentsPage} />
    <Tab.Screen name="Home" component={HomeScreen} />
  </Tab.Navigator>
);

const AppStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="MainScreen" component={MainScreen} />
      <Stack.Screen name="MessagesObjectPage" component={MessagesObjectPage} />
      <Stack.Screen
        name="SettingsUserInfo"
        component={ScreenSettingsInfoUserPage}
      />
      <Stack.Screen
        name="SettingsPassword"
        component={ScreenSettingsPassword}
      />
      <Stack.Screen
        name="NotificationsPage"
        component={ScreenNotificationsPage}
      />
    </Stack.Navigator>
  );
};

export default AppStack;
