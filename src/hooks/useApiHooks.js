/* eslint-disable prettier/prettier */
import axios from 'axios';
import {TOKEN} from '../components/authorization/LogInForm';

const URI = 'https://epilab.signumcodis.ru/api/';

export const useGetRequest = () => {
  return async (method, params = {}) => {
    const token = TOKEN;
    const headers = {
      accept: 'application/json',
    };
    if (token) {
      headers.Authorization = token;
    }
    try {
      const response = await axios.get(URI + method, {
        headers: headers,
        params: params,
      });
      return {success: true, data: response.data};
    } catch (e) {
      return {success: false, errorCode: e};
    }
  };
};

export const usePostRequest = () => {
  return async (method, postData, exceptToken = false) => {
    const token = TOKEN;
    const headers = {
      'Content-Type': 'application/json',
      accept: '*/*',
    };
    if (!exceptToken && token) {
      headers.Authorization = token;
    }
    try {
      const response = await axios.post(URI + method, postData, {
        headers: headers,
      });
      return {success: true, data: response.data};
    } catch (e) {
      return {success: false, errorCode: e};
    }
  };
};
