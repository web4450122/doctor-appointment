/* eslint-disable prettier/prettier */
import SQLite from 'react-native-sqlite-storage';

const db = SQLite.openDatabase({name: 'myDatabase.db', location: 'default'});

export const useCreateOrOpenTable = () => {
  return (tableName, columns) => {
    db.transaction(tx => {
      const columnDefinitions = columns
        .map(column => `${column.name} ${column.type}`)
        .join(', ');

      tx.executeSql(
        `CREATE TABLE IF NOT EXISTS ${tableName} (id INTEGER PRIMARY KEY AUTOINCREMENT, ${columnDefinitions})`,
        [],
        (tx, result) => {
          // console.log(`Table ${tableName} created or opened successfully`);
        },
        error => {
          console.log(`Error creating or opening table ${tableName}:`, error);
        },
      );
    });
  };
};

export const useInsertData = () => {
  return (data, fieldNames, tableName, callBack) => {
    db.transaction(tx => {
      const placeholders = Array(fieldNames.length).fill('?').join(', ');

      tx.executeSql(
        `INSERT INTO ${tableName} (${fieldNames.join(
          ', ',
        )}) VALUES (${placeholders})`,
        data,

        (tx, result) => {
          console.log('Data inserted successfully');
          callBack.forEach(callback => callback());
        },
        error => {
          console.log('Error inserting data:', error);
        },
      );
    });
  };
};

export const useUpdateData = () => {
  return (setters, fieldNames, tableName) => {
    db.transaction(tx => {
      tx.executeSql(
        `SELECT * FROM ${tableName}`,
        [],
        (tx, result) => {
          if (result.rows.length > 0) {
            const user = result.rows.item(0);

            fieldNames.forEach((fieldName, index) => {
              if (user.hasOwnProperty(fieldName) && setters[index]) {
                setters[index](user[fieldName]);
              } else {
                console.warn(
                  `Field '${fieldName}' not found in the database table.`,
                );
              }
            });
          }
        },
        error => {
          console.log('Error fetching user credentials:', error);
        },
      );
    });
  };
};

export const useUpdateDataSupplemented = () => {
  return (tableName, fieldMapping, setters) => {
    db.transaction(tx => {
      tx.executeSql(
        `SELECT * FROM ${tableName}`,
        [],
        (_, result) => {
          const data = [];
          for (let i = 0; i < result.rows.length; ++i) {
            const item = result.rows.item(i);
            const newItem = {};

            // Маппинг полей с использованием переданных данных
            Object.keys(fieldMapping).forEach(field => {
              newItem[fieldMapping[field]] = item[field];
            });

            data.push(newItem);
          }
          // console.log(data);
          setters(data);
        },
        error => {
          console.log(`Error fetching ${tableName} data:`, error);
        },
      );
    });
  };
};

export const useUpdateDataFromId = () => {
  return (tableName, id, setters, fieldNames) => {
    db.transaction(tx => {
      tx.executeSql(
        `SELECT * FROM ${tableName} WHERE id = ?`,
        [id],
        (tx, result) => {
          if (result.rows.length > 0) {
            const item = result.rows.item(0);

            fieldNames.forEach((fieldName, index) => {
              if (item.hasOwnProperty(fieldName) && setters[index]) {
                setters[index](item[fieldName]);
              } else {
                console.warn(
                  `Field '${fieldName}' not found in the database table.`,
                );
              }
            });
          }
        },
        error => {
          console.log('Error fetching medicament data:', error);
        },
      );
    });
  };
};

export const useDeleteItem = () => {
  return (tableName, id, callBack) => {
    db.transaction(tx => {
      tx.executeSql(
        `DELETE FROM ${tableName} WHERE id = ?`,
        [id],
        (tx, result) => {
          callBack.forEach(callback => callback());
          console.log('Record deleted successfully');
        },
        error => {
          console.log('Error deleting record:', error);
        },
      );
    });
  };
};

export const useDeleteTable = () => {
  return tableName => {
    db.transaction(tx => {
      tx.executeSql(
        `DROP TABLE IF EXISTS ${tableName}`,
        [],
        (tx, result) => {
          console.log('Table deleted successfully');
        },
        error => {
          console.log('Error deleting table:', error);
        },
      );
    });
  };
};

export function useSqlBuilder() {
  const openTable = useCreateOrOpenTable();
  const saveData = useInsertData();
  const updateData = useUpdateData();
  const updateDataSupplemented = useUpdateDataSupplemented();
  const updateDataFromId = useUpdateDataFromId();
  const deleteDate = useDeleteItem();
  const deleteTable = useDeleteTable();
  return {
    openTable,
    saveData,
    updateData,
    updateDataSupplemented,
    updateDataFromId,
    deleteDate,
    deleteTable,
  };
}
