/* eslint-disable prettier/prettier */
import SQLite from 'react-native-sqlite-storage';

// Сервис предназначен для дебага и удобства контроля таблиц в процессе разработки
// Используемые в нем методы не влияют на продакшен

const db = SQLite.openDatabase({name: 'myDatabase.db', location: 'default'});

export const useShowTablesBd = () => {
  return () => {
    db.transaction(tx => {
      tx.executeSql(
        "SELECT name FROM sqlite_master WHERE type='table'",
        [],
        (tx, result) => {
          const tables = [];
          for (let i = 0; i < result.rows.length; i++) {
            tables.push(result.rows.item(i).name);
          }
          console.log('Tables:', tables);
        },
        error => {
          console.log('Error fetching tables:', error);
        },
      );
    });
  };
};

export const useShowTableContent = () => {
  return tableName => {
    db.transaction(tx => {
      tx.executeSql(
        `SELECT * FROM ${tableName}`,
        [],
        (tx, result) => {
          if (result.rows.length > 0) {
            for (let i = 0; i < result.rows.length; i++) {
              const row = result.rows.item(i);
              console.log('Row:', row);
            }
          } else {
            console.log('Table is empty');
          }
        },
        error => {
          console.log('Error fetching table content:', error);
        },
      );
    });
  };
};

export const useShowShemeTable = () => {
  return tableName => {
    db.transaction(tx => {
      tx.executeSql(
        `PRAGMA table_info(${tableName})`,
        [],
        (tx, result) => {
          const rows = result.rows;
          for (let i = 0; i < rows.length; i++) {
            const column = rows.item(i);
            console.log(column.name, column.type);
          }
        },
        error => {
          console.log('Error getting table schema:', error);
        },
      );
    });
  };
};

export function useHelperTableOperations() {
  const showTables = useShowTablesBd();
  const showTableContent = useShowTableContent();
  const showShemeTable = useShowShemeTable();
  return {showTables, showTableContent, showShemeTable};
}
