/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, {useState, useEffect} from 'react';
import {View, Text, Modal, StyleSheet, TouchableOpacity} from 'react-native';
import Cbutton from '../ui/Cbutton';
import HorizontalSeparator from '../ui/HorizontalSeparator';
import {MAIN_COLOR, DARK_GREY} from '../../../App';
import {useUpdateDataSupplemented} from '../../hooks/useRequestBuilder';
import {useCreateOrOpenTable} from '../../hooks/useRequestBuilder';
import {MedicamentsFields} from '../../assets/tablesData/MedicamentsFields';
import {dataTime} from '../mainPageWidgets/MainPageReminderMedicaments';
import CiconButtons from '../ui/CiconButtons';
import SvgAccept from '../ui/icons/SvgNotifications/SvgAccept';
import SvgCancel from '../ui/icons/SvgNotifications/SvgCancel';

const ModalReminder = ({visible, cancelMove, nameTable}) => {
  const [medData, setMedData] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);

  const updateMedicamentsData = useUpdateDataSupplemented();
  const createOrOpenTable = useCreateOrOpenTable();
  createOrOpenTable('Medicaments', MedicamentsFields);

  useEffect(() => {
    updateMedicamentsData(
      'Medicaments',
      {id: 'id', preparat: 'preparat', dose: 'dose'},
      setMedData,
    );
  }, []);

  return (
    <View style={styles.root}>
      <Modal animationType="fade" transparent={true} visible={visible}>
        <View style={styles.lowerItem}>
          <View style={styles.modalItem}>
            <View style={styles.headerItem}>
              <Text style={styles.boldText}>ПРИЕМ ЛЕКАРСТВ</Text>
              <Cbutton
                styleButton={styles.buttonExit}
                colorButton={{backgroundColor: DARK_GREY}}
                name={'Закрыть'}
                onPress={cancelMove}
              />
            </View>
            <HorizontalSeparator
              Styles={styles.separator}
              Color={{backgroundColor: MAIN_COLOR}}
            />
            <View style={styles.contentItem}>
              {dataTime.map((item, index) => (
                <React.Fragment key={index}>
                  <View style={styles.cardItem}>
                    <View style={styles.timeContainer}>
                      <Text style={styles.textTime}>{item.time}</Text>
                    </View>
                    <View style={styles.medicamentContainer}>
                      {medData.map((medicament, innerIndex) => (
                        <React.Fragment key={innerIndex}>
                          <View style={styles.medicamentItem}>
                            <Text style={styles.medicamentText}>
                              {medicament.preparat}
                            </Text>
                            <Text style={styles.boldText}>16:00</Text>
                            <Text
                              style={[styles.medicamentText, {marginLeft: 40}]}>
                              {medicament.dose}
                            </Text>
                            <TouchableOpacity
                              onPress={() => setModalVisible(true)}>
                              <View
                                style={[
                                  styles.selectButton,
                                  {borderColor: MAIN_COLOR},
                                ]}
                              />
                            </TouchableOpacity>
                            <Modal
                              animationType="fade"
                              transparent={true}
                              visible={modalVisible}
                              onRequestClose={() => setModalVisible(false)}>
                              <TouchableOpacity
                                style={styles.modalBackground}
                                activeOpacity={1}
                                onPress={() => setModalVisible(false)}>
                                <View style={styles.modalContent}>
                                  <CiconButtons icon={<SvgAccept />} />
                                  <CiconButtons icon={<SvgCancel />} />
                                </View>
                              </TouchableOpacity>
                            </Modal>
                          </View>
                        </React.Fragment>
                      ))}
                    </View>
                  </View>
                  {index !== dataTime.length - 1 && (
                    <HorizontalSeparator Styles={styles.separatorTwo} />
                  )}
                </React.Fragment>
              ))}
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  lowerItem: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
  },
  modalItem: {
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 20,
    height: 280,
    width: 340,
    borderRadius: 22,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  headerItem: {
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 30,
    width: '95%',
    flexDirection: 'row',
  },
  separator: {
    height: 2,
    width: '100%',
  },
  separatorTwo: {
    height: 1,
    width: '100%',
  },
  boldText: {
    color: 'black',
    textAlign: 'center',
  },
  lightText: {
    marginTop: '5%',
    textAlign: 'center',
    color: 'black',
  },
  textTime: {
    textAlign: 'center',
    marginBottom: 5,
    color: 'black',
  },
  medicamentText: {
    color: 'black',
  },
  contentItem: {
    height: '90%',
    width: '100%',
    marginTop: 8,
  },
  cardItem: {
    flexDirection: 'row',
    height: 30,
    marginTop: 5,
  },
  timeContainer: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    marginRight: 8,
    height: 35,
    width: 40,
    backgroundColor: 'transparent', // #F9F9F9
    // borderWidth: 2,
  },
  medicamentContainer: {
    flex: 1,
    flexDirection: 'column',
  },
  medicamentItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 5,
    // borderWidth: 2,
  },
  selectButton: {
    height: 20,
    width: 20,
    borderRadius: 10,
    borderWidth: 2,
  },
  modalBackground: {
    flex: 1,
    // backgroundColor: 'rgba(0, 0, 0, 0.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalContent: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: 'white',
    width: 120,
    padding: 10,
    borderRadius: 14,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 3,
  },
  buttonExit: {
    height: 24,
    width: 90,
    marginTop: 6,

    borderRadius: 20,
  },
});

export default ModalReminder;
