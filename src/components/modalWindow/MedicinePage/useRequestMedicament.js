/* eslint-disable prettier/prettier */
import SQLite from 'react-native-sqlite-storage';

const db = SQLite.openDatabase({name: 'myDatabase.db', location: 'default'});

export const useReplaceCard = () => {
  return (dependencies, callBack) => {
    db.transaction(tx => {
      tx.executeSql(
        'INSERT OR REPLACE INTO Medicaments (id, preparat, dose, rotationDay, startUse, endUse, reminderOne, reminderTwo, reminderThree, comment) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
        [...dependencies],
        (tx, result) => {
          if (callBack) {
            callBack.forEach(callback => callback());
          }
          console.log('Карточка записи лекарств обновлена');
        },
        error => {
          console.log('Error updating medicaments:', error);
        },
      );
    });
  };
};
