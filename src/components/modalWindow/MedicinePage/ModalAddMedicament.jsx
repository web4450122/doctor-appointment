/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Modal,
  TextInput,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Cbutton from '../../ui/Cbutton';
import {MAIN_COLOR, DARK_GREY, INACTIVE_GREY} from '../../../../App';
import {useReplaceCard} from './useRequestMedicament';
import {
  MedicamentsScheme,
  MedicamentsFields,
} from '../../../assets/tablesData/MedicamentsFields';
import {useSqlBuilder} from '../../../hooks/useRequestBuilder';
import {checkFieldsIsEmpty} from '../../../assets/checkField/CheckFieldsIsEmpty';
import DateTimePicker from '@react-native-community/datetimepicker';
import {rerenderComponent} from '../../../assets/render/RerenderComponent';

const ModalAddMedicament = ({
  visible,
  visibleModalEditer,
  reWriteStateInTable,
  closeMove,
  current_id,
  updateMedicamentsData,
}) => {
  const {openTable, saveData, updateDataFromId} = useSqlBuilder();
  openTable('Medicaments', MedicamentsScheme);
  const replaceDataCard = useReplaceCard();

  const [date, setDate] = useState(new Date());
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);
  const [updateDateOrTime, setUpdateDateOrTime] = useState(false);
  const [phase, setPhase] = useState('all');

  const [preparat, setPreparat] = useState('');
  const [dose, setDose] = useState('');
  const [rotationDay, setRotationDay] = useState('3');
  const [startUse, setStartUse] = useState();
  const [endUse, setEndUse] = useState();
  const [reminderOne, setReminderOne] = useState();
  const [reminderTwo, setReminderTwo] = useState();
  const [reminderThree, setReminderThree] = useState();
  const [comment, setComment] = useState('');

  const [isPreparatEmpty, setIsPreparatEmpty] = useState(false);
  const [isDoseEmpty, setIsDoseEmpty] = useState(false);

  const onChange = (event, selectedDate) => {
    const nowDate = selectedDate;
    setShow(false);
    setDate(nowDate);
  };

  function updateDate() {
    const days = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();
    const months =
      date.getMonth() < 10 ? `0${date.getMonth() + 1}` : date.getMonth();
    if (phase === '1') {
      setStartUse(`${days}.${months}.${date.getFullYear()}`);
    } else if (phase === '2') {
      setEndUse(`${days}.${months}.${date.getFullYear()}`);
    } else {
      setStartUse(`${days}.${months}.${date.getFullYear()}`);
      setEndUse(`${days}.${months}.${date.getFullYear()}`);
    }
  }

  function updateTime() {
    const hours =
      date.getHours() < 10 ? `0${date.getHours()}` : date.getHours();
    const minutes =
      date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes();
    if (phase === '1') {
      setReminderOne(`${hours}:${minutes}`);
    } else if (phase === '2') {
      setReminderTwo(`${hours}:${minutes}`);
    } else if (phase === '3') {
      setReminderThree(`${hours}:${minutes}`);
    } else {
      setReminderOne(`${hours}:${minutes}`);
      setReminderTwo(`${hours}:${minutes}`);
      setReminderThree(`${hours}:${minutes}`);
    }
  }

  useEffect(() => {
    updateDateOrTime ? updateTime() : updateDate();
  }, [date]);

  useEffect(() => {
    updateTime();
    updateDate();
  }, []);

  const showMode = currentMode => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = numberFiled => {
    showMode('date');
    setUpdateDateOrTime(false);
    setPhase(numberFiled);
  };

  const showTimepicker = numberFiled => {
    showMode('time');
    setUpdateDateOrTime(true);
    setPhase(numberFiled);
  };

  const fieldsToCheck = [
    {value: preparat, setter: setIsPreparatEmpty},
    {value: dose, setter: setIsDoseEmpty},
  ];

  // функция для сброса всех полей, используется при нажатие на кнопку отмена
  function resetFieldsInModal() {
    setPreparat('');
    setDose('');
    setIsPreparatEmpty(false);
    setIsDoseEmpty(false);
  }

  useEffect(() => {
    if (!visibleModalEditer) {
      resetFieldsInModal();
    }
    if (!visible) {
      resetFieldsInModal();
    }
  }, [visible, visibleModalEditer]);

  const reWriteNote = () => {
    if (reWriteStateInTable) {
      replaceDataCard(
        [
          current_id,
          preparat,
          dose,
          rotationDay,
          startUse,
          endUse,
          reminderOne,
          reminderTwo,
          reminderThree,
          comment,
        ],
        [updateMedicamentsData, resetFieldsInModal],
      );
      rerenderComponent();
    }
  };

  // Если поля, требующие заполнения пустые, то по нажатию на кнопку сохранить, переменные по контролю полей переводятся в true и поля горят красным, если поля запонены, то переменные переключаются в false
  const saveMedicaments = () => {
    checkFieldsIsEmpty(fieldsToCheck);
    if (preparat.length > 0 && dose.length > 0) {
      saveData(
        [
          preparat,
          dose,
          rotationDay,
          startUse,
          endUse,
          reminderOne,
          reminderTwo,
          reminderThree,
          comment,
        ],
        MedicamentsFields,
        'Medicaments',
        [updateMedicamentsData, resetFieldsInModal],
      );
      rerenderComponent();
    }
  };

  // При нажатие на редактировать, мы получаем айди карточки и грузим данные в модальное окно из бд по айди
  useEffect(() => {
    updateDataFromId(
      'Medicaments',
      current_id,
      [
        setPreparat,
        setDose,
        setRotationDay,
        setStartUse,
        setEndUse,
        setReminderOne,
        setReminderTwo,
        setReminderThree,
        setComment,
      ],
      MedicamentsFields,
    );
  }, [current_id]);

  return (
    <View style={styles.root}>
      <Modal animationType="fade" transparent={true} visible={visible}>
        <View style={styles.lowerItem}>
          <View style={styles.modalWindowBackPlaid}>
            <View style={styles.headerName}>
              <Text style={styles.boldText}>Препарат</Text>
            </View>

            <View style={styles.itemOne}>
              <TextInput
                style={[
                  styles.inputPreparat,
                  {
                    borderWidth: 1,
                    borderColor: isPreparatEmpty ? 'red' : 'transparent',
                  },
                ]}
                placeholderTextColor={INACTIVE_GREY}
                placeholder=""
                value={preparat}
                onChangeText={text => setPreparat(text)}
              />
            </View>

            <View style={styles.itemTwo}>
              <View style={{width: 120}}>
                <Text style={styles.textLight}>Дозировка,{'\n'}мг/мл</Text>
                <TextInput
                  style={[
                    styles.inputDose,
                    {
                      borderWidth: 1,
                      borderColor: isDoseEmpty ? 'red' : 'transparent',
                    },
                  ]}
                  placeholderTextColor={INACTIVE_GREY}
                  placeholder=""
                  value={dose}
                  keyboardType="numeric"
                  onChangeText={text => setDose(text)}
                />
              </View>

              <View
                style={{
                  width: 160,
                }}>
                <Text style={styles.textLight}>
                  Переодичность приема,{'\n'}раз в сутки
                </Text>
                <TextInput
                  style={styles.inputRotation}
                  placeholderTextColor={INACTIVE_GREY}
                  placeholder=""
                  value={rotationDay}
                  editable={false}
                  onChangeText={text => setRotationDay(text)}
                />
              </View>
            </View>

            <View style={styles.itemThree}>
              <View style={{width: 120}}>
                <Text style={styles.textLight}>Начало</Text>
                <TouchableOpacity onPress={() => showDatepicker('1')}>
                  <View style={styles.inputStartUse}>
                    <Text style={styles.textLight}>{startUse}</Text>
                  </View>
                </TouchableOpacity>
              </View>

              <View
                style={{
                  width: 160,
                }}>
                <Text style={styles.textStop}>Окончание</Text>
                <TouchableOpacity onPress={() => showDatepicker('2')}>
                  <View style={styles.inputStartUse}>
                    <Text style={styles.textLight}>{endUse}</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
            {show && (
              <DateTimePicker
                testID="dateTimePicker"
                value={date}
                mode={mode}
                is24Hour={true}
                onChange={(event, selectedDate) => {
                  if (event.type === 'set') {
                    onChange(event, selectedDate);
                    console.log('Дата изменена:', selectedDate);
                  } else if (event.type === 'dismissed') {
                    console.log('Пикер закрыт на кнопку отмены');
                    setShow(false);
                  }
                }}
              />
            )}
            <View style={styles.itemFour}>
              <View style={{width: '100%'}}>
                <Text style={styles.textLight}>Напоминания</Text>
                <View style={{width: '100%', flexDirection: 'row'}}>
                  <TouchableOpacity
                    onPress={() => showTimepicker('1')}
                    style={{width: '33%'}}>
                    <View style={styles.reminderItem}>
                      <Text style={styles.textLight}>{reminderOne}</Text>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => showTimepicker('2')}
                    style={{width: '33%'}}>
                    <View style={styles.reminderItem}>
                      <Text style={styles.textLight}>{reminderTwo}</Text>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => showTimepicker('3')}
                    style={{width: '33%'}}>
                    <View style={styles.reminderItem}>
                      <Text style={styles.textLight}>{reminderThree}</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            <View style={styles.itemFive}>
              <Text style={styles.textLight}>Примечания</Text>
              <TextInput
                style={styles.inputDescription}
                placeholder=""
                value={comment}
                editable={true}
                onChangeText={text => setComment(text)}
              />
            </View>

            <View style={styles.buttonsItem}>
              <Cbutton
                styleButton={styles.buttonCancel}
                colorButton={{backgroundColor: DARK_GREY}}
                name={'Отмена'}
                onPress={() => {
                  closeMove();
                  resetFieldsInModal();
                }}
              />
              <Cbutton
                styleButton={styles.buttonSave}
                colorButton={{backgroundColor: MAIN_COLOR}}
                name={'Сохранить'}
                onPress={() =>
                  reWriteStateInTable ? reWriteNote() : saveMedicaments()
                }
              />
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  lowerItem: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
  },
  modalWindowBackPlaid: {
    //   justifyContent: 'center',
    //   alignItems: 'center',
    backgroundColor: '#e6e6e6',
    padding: 20,
    height: 360,
    width: 330,
    borderRadius: 22,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  headerName: {
    width: '100%',
    // borderWidth: 2,
  },
  boldText: {
    textAlign: 'left',
    fontWeight: 'bold',
    color: 'black',
  },
  textLight: {
    color: 'black',
  },
  textStop: {
    textAlign: 'left',
    color: 'black',
  },
  itemOne: {
    // borderWidth: 2,
    width: '100%',
    marginTop: 1,
  },
  itemTwo: {
    width: '100%',
    // borderWidth: 2,
    // borderColor: 'red',
    marginTop: 1,
    flexDirection: 'row',
  },
  itemThree: {
    width: '100%',
    // borderWidth: 2,
    // borderColor: 'red',
    marginTop: 1,
    flexDirection: 'row',
  },
  itemFour: {
    width: '100%',
    // borderWidth: 2,
    // borderColor: 'red',
    marginTop: 1,
    flexDirection: 'row',
  },
  itemFive: {
    width: '100%',
    // borderWidth: 2,
    // borderColor: 'red',
    marginTop: 1,
    flexDirection: 'column',
  },
  inputPreparat: {
    color: 'black',
    justifyContent: 'center',
    alignItems: 'flex-start',
    height: 24,
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 2,
    margin: 3,
    paddingVertical: 3,
    paddingHorizontal: 10,
    // marginTop: 0,
  },
  inputDose: {
    color: 'black',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    height: 24,
    width: '40%',
    backgroundColor: 'white',
    borderRadius: 7,
    margin: 3,
    paddingVertical: 3,
    paddingHorizontal: 10,
  },
  inputRotation: {
    color: 'black',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    height: 24,
    width: '90%',
    backgroundColor: 'white',
    borderRadius: 7,
    margin: 3,
    paddingVertical: 3,
    paddingHorizontal: 10,
  },
  inputStartUse: {
    color: 'black',
    justifyContent: 'center',
    alignItems: 'flex-start',
    height: 24,
    width: '83%',
    backgroundColor: 'white',
    borderRadius: 7,
    margin: 3,
    paddingVertical: 3,
    paddingHorizontal: 10,
  },
  inputEndUse: {
    color: 'black',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    height: 24,
    width: '80%',
    backgroundColor: 'white',
    borderRadius: 7,
    margin: 3,
    paddingVertical: 3,
    paddingHorizontal: 10,
  },
  inputDescription: {
    color: 'black',
    justifyContent: 'center',
    alignItems: 'center',
    height: 35,
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 7,
    margin: 3,
    paddingVertical: 3,
    paddingHorizontal: 10,
  },
  reminderItem: {
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    height: 24,
    // width: '30%',
    backgroundColor: 'white',
    borderRadius: 7,
    margin: 3,
    paddingVertical: 3,
    paddingHorizontal: 10,
    // borderWidth: 2,
  },
  buttonsItem: {
    display: 'flex',
    // borderWidth: 2,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 5,
    left: 0,
    right: 0,
    flexDirection: 'row',
  },
  buttonCancel: {
    height: 38,
    width: 110,
    borderRadius: 20,
  },
  buttonSave: {
    height: 38,
    width: 110,
    borderRadius: 20,
  },
});

export default ModalAddMedicament;
