/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, {useState, useEffect} from 'react';
import {View, Text, Modal, StyleSheet} from 'react-native';
import CheckBox from '../ui/CheckBox';

const ModalList = ({visible, list, onModalClose, onCheckboxSelect}) => {
  const [checkedItems, setCheckedItems] = useState(
    new Array(list.length).fill(false),
  );
  const [modalOpened, setModalOpened] = useState(false);
  // еслм modalOpened равен false то окно не закроется, если был выбран чекс бокс, то переменная -
  // становится true и окно закрывается, после закртия окна, переменная обратно возвращается в false

  const handleCheckboxToggle = index => {
    const updatedCheckedItems = [...checkedItems];

    // Снимаем предыдущий выбранный чекбокс, если он был выбран
    const previouslyCheckedIndex = updatedCheckedItems.findIndex(
      item => item === true,
    );
    if (previouslyCheckedIndex !== -1 && previouslyCheckedIndex !== index) {
      updatedCheckedItems[previouslyCheckedIndex] = false;
    }

    // Устанавливаем новое значение для выбранного чекбокса
    updatedCheckedItems[index] = !updatedCheckedItems[index];
    setCheckedItems(updatedCheckedItems);

    // Если чекбокс выбран, передаем текст в родительский компонент
    if (updatedCheckedItems[index]) {
      onCheckboxSelect(list[index]);
    }
  };

  useEffect(() => {
    // Проверяем, есть ли хотя бы один выбранный чекбокс
    const atLeastOneChecked = checkedItems.some(item => item === true);

    // console.log(atLeastOneChecked, modalOpened);
    // Если есть хотя бы один выбранный чекбокс, закрываем модальное окно
    if (atLeastOneChecked && modalOpened) {
      // Закрыть модальное окно через функцию обратного вызова
      onModalClose();
      setModalOpened(false);
    }
  }, [checkedItems, onModalClose]);

  return (
    <View style={styles.root}>
      <Modal animationType="fade" transparent={true} visible={visible}>
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            {list.map((item, index) => (
              <CheckBox
                key={index}
                label={item}
                checked={checkedItems[index]}
                onPress={() => {
                  handleCheckboxToggle(index);
                  setModalOpened(true);
                }}
              />
            ))}
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalContent: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'column',
    flexWrap: 'wrap',
    paddingTop: 10,
    paddingLeft: 10,
    backgroundColor: 'white',
    height: 360,
    width: 330,
    borderRadius: 22,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
});

export default ModalList;
