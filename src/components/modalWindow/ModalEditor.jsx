/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React from 'react';
import {View, Text, Modal, Image, StyleSheet} from 'react-native';
import Cbutton from '../ui/Cbutton';
import {MAIN_COLOR, DARK_GREY} from '../../../App';

const ModalEditor = ({visible, closeMove, editMove, deleteMove}) => {
  return (
    <View style={styles.root}>
      <Modal animationType="fade" transparent={true} visible={visible}>
        <View style={styles.lowerItem}>
          <View style={styles.modalItem}>
            <View style={styles.columnButtons}>
              <Cbutton
                styleButton={styles.buttonCancel}
                colorButton={{backgroundColor: MAIN_COLOR}}
                name={'Отмена'}
                onPress={closeMove}
              />
              <Cbutton
                styleButton={styles.buttonEditor}
                colorButton={{backgroundColor: MAIN_COLOR}}
                name={'Редактировать'}
                onPress={editMove}
              />
              <Cbutton
                styleButton={styles.buttonDelete}
                colorButton={{backgroundColor: DARK_GREY}}
                name={'Удалить'}
                onPress={deleteMove}
              />
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  lowerItem: {
    // position: 'absolute',
    // top: 20,
    // right: 20,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalItem: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',

    height: 148,
    width: 120,
    borderRadius: 22,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  columnButtons: {
    display: 'flex',
    flexDirection: 'column',
    marginTop: 4,
  },
  buttonCancel: {
    height: 38,
    width: 110,

    borderRadius: 20,
  },
  buttonEditor: {
    height: 38,
    width: 110,

    borderRadius: 20,
  },
  buttonDelete: {
    height: 38,
    width: 110,

    borderRadius: 20,
  },
});

export default ModalEditor;
