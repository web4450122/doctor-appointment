import React from 'react';
import {View, Text, Modal, Image, StyleSheet} from 'react-native';
import Cbutton from '../ui/Cbutton';
import {MAIN_COLOR, INACTIVE_GREY} from '../../../App';

const AlertModal = ({visible, closeMove}) => {
  return (
    <View style={styles.root}>
      <Modal animationType="fade" transparent={true} visible={visible}>
        <View style={styles.lowerItem}>
          <View style={styles.modalItem}>
            <Image
              source={require('../../assets/image/Union.png')}
              style={{width: 60, height: 90}}
              resizeMode="contain"
            />
            <Text style={[styles.text, {color: INACTIVE_GREY}]}>
              Это усеченное приложение и для полной версии приложения просьба
              обратиться в клинику
            </Text>
            <Cbutton
              styleButton={styles.buttonExit}
              colorButton={{backgroundColor: MAIN_COLOR}}
              name={'Закрыть'}
              onPress={closeMove}
            />
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  lowerItem: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
  },
  modalItem: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 20,
    height: 280,
    width: 300,
    borderRadius: 22,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  text: {
    marginTop: '10%',
    textAlign: 'center',
  },
  buttonExit: {
    height: 38,
    width: 110,

    borderRadius: 20,
    marginTop: 30,
  },
});

export default AlertModal;
