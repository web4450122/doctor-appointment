/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React from 'react';
import {View, Text, Modal, Image, StyleSheet} from 'react-native';
import Cbutton from '../ui/Cbutton';
import {MAIN_COLOR, DARK_GREY, INACTIVE_GREY} from '../../../App';
import {useDeleteItem} from '../../hooks/useRequestBuilder';
import {rerenderComponent} from '../../assets/render/RerenderComponent';

const ModalDeleteAlert = ({
  visible,
  cancelMove,
  nameTable,
  id,
  updateCardAfterDelete,
}) => {
  const deleteCard = useDeleteItem();

  const handleDelete = closeWindow => {
    deleteCard(nameTable, id, [updateCardAfterDelete]); // Используем id для удаления записи
    rerenderComponent();
    closeWindow();
  };

  return (
    <View style={styles.root}>
      <Modal animationType="fade" transparent={true} visible={visible}>
        <View style={styles.lowerItem}>
          <View style={styles.modalItem}>
            <Image
              source={require('../../assets/image/Illustration.png')}
              style={{width: 100, height: 70}}
              resizeMode="contain"
            />
            <Text style={[styles.boldText, {color: DARK_GREY}]}>
              Точно ли Вы хотите удалить?
            </Text>
            <Text style={[styles.lightText, {color: INACTIVE_GREY}]}>
              После удаления, информация будет искажена и это помешает доктору
              сделать правильные выводы
            </Text>
            <View style={styles.rowButtons}>
              <Cbutton
                styleButton={styles.buttonCancel}
                colorButton={{backgroundColor: DARK_GREY}}
                name={'Отмена'}
                onPress={cancelMove}
              />
              <Cbutton
                styleButton={styles.buttonDelete}
                colorButton={{backgroundColor: MAIN_COLOR}}
                name={'Удалить'}
                onPress={() => handleDelete(cancelMove)}
              />
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  lowerItem: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
  },
  modalItem: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 20,
    height: 280,
    width: 300,
    borderRadius: 22,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  boldText: {
    marginTop: '8%',
    textAlign: 'center',
    fontWeight: 'bold',
  },
  lightText: {
    marginTop: '5%',
    textAlign: 'center',
  },
  rowButtons: {
    display: 'flex',
    flexDirection: 'row',
  },
  buttonCancel: {
    height: 38,
    width: 110,
    marginTop: 30,

    borderRadius: 20,
  },
  buttonDelete: {
    height: 38,
    width: 110,
    marginTop: 30,

    borderRadius: 20,
  },
});

export default ModalDeleteAlert;
