/* eslint-disable prettier/prettier */
import SQLite from 'react-native-sqlite-storage';

const db = SQLite.openDatabase({name: 'myDatabase.db', location: 'default'});

export const useReplaceCard = () => {
  return (dependencies, callBack) => {
    db.transaction(tx => {
      tx.executeSql(
        'INSERT OR REPLACE INTO Seizures (id, seizure, currentDate, currentTime, continueMinutes, sleepState, comment) VALUES (?, ?, ?, ?, ?, ?, ?)',
        [...dependencies],
        (tx, result) => {
          if (callBack) {
            callBack.forEach(callback => callback());
          }
          console.log('Карточка приступов обновлена');
        },
        error => {
          console.log('Error updating seizures:', error);
        },
      );
    });
  };
};
