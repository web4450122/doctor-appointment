/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Modal,
  TextInput,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Cbutton from '../../ui/Cbutton';
import SwitchToggle from '../../ui/SwitchToggle';
import CiconButtons from '../../ui/CiconButtons';
import SvgAttach from '../../ui/icons/SvgChatPage/SvgAttach';
import ModalList from '../ModalList';
import {MAIN_COLOR, DARK_GREY, INACTIVE_GREY} from '../../../../App';
import {
  seizureScheme,
  seizureFields,
} from '../../../assets/tablesData/SeizuresFields';
import {useSqlBuilder} from '../../../hooks/useRequestBuilder';
import {useReplaceCard} from './useRequestSeizures';
import {checkFieldsIsEmpty} from '../../../assets/checkField/CheckFieldsIsEmpty';
import DateTimePicker from '@react-native-community/datetimepicker';
import {rerenderComponent} from '../../../assets/render/RerenderComponent';
import {resetFields} from '../ModalDeleteAlert';

const ModalAddSeizures = ({
  visible,
  visibleModalEditer,
  reWriteStateInTable,
  closeMove,
  current_id,
  updateSeizurestData,
}) => {
  const {openTable, saveData, updateDataFromId} = useSqlBuilder();
  openTable('Seizures', seizureScheme);
  const replaceDataCard = useReplaceCard();

  const data = require('../../../assets/seizures/seizures.json');
  const listSeizures = data.seizures || [];

  const [date, setDate] = useState(new Date());
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);
  const [updateDateOrTime, setUpdateDateOrTime] = useState(false);

  const [seizure, setSeizure] = useState('');
  const [currentDate, setCurrentDate] = useState();
  const [currentTime, setCurrentTime] = useState();
  const [continueMinutes, setContinueMinutes] = useState('');
  const [comment, setComment] = useState('');

  const [isLeftSwitchToggle, setIsLeftSwitchToggle] = useState(1);
  const [visibleModalList, setVisibleModalList] = useState(false);

  const [isSeizureEmpty, setIsSeizureEmpty] = useState(false);
  const [isContinueMinutesEmpty, setIsContinueMinutesEmpty] = useState(false);

  const onChange = (event, selectedDate) => {
    const nowDate = selectedDate;
    setShow(false);
    setDate(nowDate);
  };

  function updateDate() {
    const days = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();
    const months =
      date.getMonth() < 10 ? `0${date.getMonth() + 1}` : date.getMonth();
    setCurrentDate(`${days}.${months}.${date.getFullYear()}`);
  }

  function updateTime() {
    const hours =
      date.getHours() < 10 ? `0${date.getHours()}` : date.getHours();
    const minutes =
      date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes();
    setCurrentTime(`${hours}:${minutes}`);
  }

  useEffect(() => {
    updateDateOrTime ? updateTime() : updateDate();
  }, [date]);

  useEffect(() => {
    updateTime();
    updateDate();
  }, []);

  const showMode = currentMode => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
    setUpdateDateOrTime(false);
  };

  const showTimepicker = () => {
    showMode('time');
    setUpdateDateOrTime(true);
  };

  const fieldsToCheck = [
    {value: seizure, setter: setIsSeizureEmpty},
    {value: continueMinutes, setter: setIsContinueMinutesEmpty},
  ];

  function resetFieldsInModal() {
    setSeizure('');
    setContinueMinutes('');
    setIsSeizureEmpty(false);
    setIsContinueMinutesEmpty(false);
  }

  useEffect(() => {
    if (!visibleModalEditer) {
      resetFieldsInModal();
    }
    if (!visible) {
      resetFieldsInModal();
    }
  }, [visible, visibleModalEditer]);

  const closeModal = () => {
    // Обновление состояния видимости модального окна для его закрытия
    setVisibleModalList(false);
  };
  const openModal = () => {
    setVisibleModalList(true);
  };

  const handleSwitchToggle = () => {
    setIsLeftSwitchToggle(!isLeftSwitchToggle);
  };

  const handleCheckboxSelect = text => {
    setSeizure(text);
    // метод записывает в переменную, выбраную болезнь из списка
  };

  const reWriteNote = () => {
    if (reWriteStateInTable) {
      replaceDataCard(
        [
          current_id,
          seizure,
          currentDate,
          currentTime,
          continueMinutes,
          isLeftSwitchToggle,
          comment,
        ],
        [updateSeizurestData, resetFieldsInModal],
      );
      rerenderComponent();
    }
  };

  const saveSeizures = () => {
    checkFieldsIsEmpty(fieldsToCheck);
    if (seizure.length > 0 && continueMinutes.length > 0) {
      saveData(
        [
          seizure,
          currentDate,
          currentTime,
          continueMinutes,
          isLeftSwitchToggle,
          comment,
        ],
        seizureFields,
        'Seizures',
        [updateSeizurestData, resetFieldsInModal],
      );
      rerenderComponent();
    }
  };

  useEffect(() => {
    console.log(isLeftSwitchToggle);
  }, []);

  useEffect(() => {
    updateDataFromId(
      'Seizures',
      current_id,
      [
        setSeizure,
        setCurrentDate,
        setCurrentTime,
        setContinueMinutes,
        setIsLeftSwitchToggle,
        setComment,
      ],
      seizureFields,
    );
  }, [current_id]);

  return (
    <View style={styles.root}>
      <Modal
        animationType="fade"
        transparent={true}
        visible={visible}
        onModalClose={closeModal}>
        <View style={styles.lowerItem}>
          <View style={styles.modalWindowBackPlaid}>
            {show && (
              <DateTimePicker
                testID="dateTimePicker"
                value={date}
                mode={mode}
                is24Hour={true}
                onChange={(event, selectedDate) => {
                  if (event.type === 'set') {
                    onChange(event, selectedDate);
                    console.log('Дата изменена:', selectedDate);
                  } else if (event.type === 'dismissed') {
                    console.log('Пикер закрыт на кнопку отмены');
                    setShow(false);
                  }
                }}
              />
            )}
            <View style={styles.headerName}>
              <Text style={[styles.textBold, {color: DARK_GREY}]}>
                Вид приступа
              </Text>
            </View>

            <View style={styles.itemOne}>
              <ModalList
                visible={visibleModalList}
                list={listSeizures}
                onModalClose={closeModal}
                onCheckboxSelect={handleCheckboxSelect}
              />
              <TouchableOpacity onPress={() => openModal()}>
                <View
                  style={[
                    styles.selectSeizureItem,
                    {
                      borderWidth: 1,
                      borderColor: isSeizureEmpty ? 'red' : 'transparent',
                    },
                  ]}>
                  <Text style={styles.seizureText}>{seizure}</Text>
                  <View style={styles.arrowItem}>
                    <Text style={[styles.arrowTextSymbol, {color: MAIN_COLOR}]}>
                      {'>'}
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>

            <View style={styles.itemTwo}>
              <View style={{width: 120}}>
                <Text style={styles.textLight}>Дата</Text>
                <TouchableOpacity onPress={showDatepicker}>
                  <View style={styles.inputDate}>
                    <Text style={styles.textLight}>{currentDate}</Text>
                  </View>
                </TouchableOpacity>
              </View>

              <View
                style={{
                  width: 160,
                  marginLeft: 40,
                }}>
                <Text style={styles.textLight}>Время</Text>
                <TouchableOpacity onPress={showTimepicker}>
                  <View style={styles.inputRotation}>
                    <Text style={styles.textLight}>{currentTime}</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>

            <View style={styles.itemThree}>
              <View style={styles.continueItem}>
                <Text style={styles.textLight}>Продолжительность</Text>
                <View style={styles.continueItemLower}>
                  <TextInput
                    style={[
                      styles.inputContinue,
                      {
                        borderWidth: 1,
                        borderColor: isContinueMinutesEmpty
                          ? 'red'
                          : 'transparent',
                      },
                    ]}
                    placeholderTextColor={INACTIVE_GREY}
                    placeholder=""
                    value={continueMinutes}
                    keyboardType="numeric"
                    onChangeText={text => setContinueMinutes(text)}
                  />
                  <Text style={styles.textMinute}>Минут</Text>
                </View>
              </View>
              <View
                style={{
                  width: 160,
                  marginLeft: 12,
                  marginTop: 2,
                }}>
                <Text style={styles.textInSleep}>Во сне</Text>
                <SwitchToggle
                  isLeft={isLeftSwitchToggle}
                  setIsLeft={handleSwitchToggle}
                />
              </View>
            </View>

            <View style={styles.itemFour}>
              <Text style={styles.textLight}>Примечания</Text>
              <TextInput
                style={styles.inputDescription}
                placeholderTextColor={INACTIVE_GREY}
                placeholder=""
                value={comment}
                editable={true}
                onChangeText={text => setComment(text)}
              />
            </View>

            <View style={styles.itemFive}>
              <Text style={styles.textLight}>Вложения</Text>
              <CiconButtons icon={<SvgAttach />} />
            </View>

            <View style={styles.buttonsItem}>
              <Cbutton
                styleButton={styles.buttonCancel}
                colorButton={{backgroundColor: DARK_GREY}}
                name={'Отмена'}
                onPress={() => {
                  closeMove();
                  resetFieldsInModal();
                }}
              />
              <Cbutton
                styleButton={styles.buttonSave}
                colorButton={{backgroundColor: MAIN_COLOR}}
                name={'Сохранить'}
                onPress={() =>
                  reWriteStateInTable ? reWriteNote() : saveSeizures()
                }
              />
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  lowerItem: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
  },
  modalWindowBackPlaid: {
    //   justifyContent: 'center',
    //   alignItems: 'center',
    backgroundColor: '#e6e6e6',
    padding: 20,
    height: 360,
    width: 330,
    borderRadius: 22,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  headerName: {
    width: '100%',
    // borderWidth: 2,
  },
  textMinute: {
    marginHorizontal: 10,
    color: 'black',
  },
  textInSleep: {
    textAlign: 'left',
    color: 'black',
  },
  textBold: {
    textAlign: 'left',
    fontWeight: 'bold',
  },
  textLight: {
    color: 'black',
  },
  seizureText: {
    marginVertical: -2,
    color: 'black',
  },
  arrowItem: {
    position: 'absolute',
    top: -1,
    right: 5,
    transform: 'rotate(90deg)',
  },
  arrowTextSymbol: {
    textAlign: 'center',
    fontSize: 20,
  },
  itemOne: {
    // borderWidth: 2,
    width: '100%',
    marginTop: 1,
  },
  itemTwo: {
    width: '100%',
    // borderWidth: 2,
    // borderColor: 'red',
    marginTop: 1,
    flexDirection: 'row',
  },
  itemThree: {
    width: '100%',
    // borderWidth: 2,
    // borderColor: 'red',
    marginTop: 1,
    flexDirection: 'row',
  },
  continueItem: {
    width: 150,
    flexDirection: 'column',
  },
  continueItemLower: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
    // borderWidth: 2,
  },
  itemFour: {
    width: '100%',
    // borderWidth: 2,
    // borderColor: 'red',
    marginTop: 1,
    flexDirection: 'column',
  },
  itemFive: {
    width: '100%',
    // borderWidth: 2,
    // borderColor: 'red',
    marginTop: 1,
    flexDirection: 'column',
  },
  selectSeizureItem: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    height: 24,
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 7,
    margin: 3,
    paddingVertical: 3,
    paddingHorizontal: 10,
    // marginTop: 0,
  },
  inputDate: {
    color: 'black',
    justifyContent: 'center',
    alignItems: 'flex-start',
    height: 24,
    width: '90%',
    backgroundColor: 'white',
    borderRadius: 7,
    margin: 3,
    paddingVertical: 3,
    paddingHorizontal: 10,
  },
  inputRotation: {
    color: 'black',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    height: 24,
    width: '60%',
    backgroundColor: 'white',
    borderRadius: 7,
    margin: 3,
    paddingVertical: 3,
    paddingHorizontal: 10,
  },
  inputContinue: {
    color: 'black',
    justifyContent: 'center',
    alignItems: 'flex-start',
    height: 24,
    width: '24%',
    backgroundColor: 'white',
    borderRadius: 7,
    margin: 3,
    paddingVertical: 3,
    paddingHorizontal: 10,
  },
  inputEndUse: {
    color: 'black',
    justifyContent: 'center',
    alignItems: 'center',
    height: 24,
    width: '60%',
    backgroundColor: 'white',
    borderRadius: 7,
    margin: 3,
    paddingVertical: 3,
    paddingHorizontal: 10,
  },
  inputDescription: {
    color: 'black',
    justifyContent: 'center',
    alignItems: 'center',
    height: 35,
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 7,
    margin: 3,
    paddingVertical: 3,
    paddingHorizontal: 10,
  },
  reminderItem: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    height: 24,
    width: '30%',
    backgroundColor: 'white',
    borderRadius: 7,
    margin: 3,
    paddingVertical: 3,
    paddingHorizontal: 10,
  },
  buttonsItem: {
    display: 'flex',
    // borderWidth: 2,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 5,
    left: 0,
    right: 0,
    flexDirection: 'row',
  },
  buttonCancel: {
    height: 38,
    width: 110,
    borderRadius: 20,
  },
  buttonSave: {
    height: 38,
    width: 110,
    borderRadius: 20,
  },
});

export default ModalAddSeizures;
