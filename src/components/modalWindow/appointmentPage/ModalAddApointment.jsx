/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Modal,
  TextInput,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Cbutton from '../../ui/Cbutton';
import {MAIN_COLOR, DARK_GREY, INACTIVE_GREY} from '../../../../App';
import {useReplaceCard} from './useRequestAppointments';
import {
  appointmentScheme,
  appointmentFields,
} from '../../../assets/tablesData/AppointmentFields';
import {checkFieldsIsEmpty} from '../../../assets/checkField/CheckFieldsIsEmpty';
import {useSqlBuilder} from '../../../hooks/useRequestBuilder';
import {rerenderComponent} from '../../../assets/render/RerenderComponent';
import DateTimePicker from '@react-native-community/datetimepicker';
import {resetFields} from '../ModalDeleteAlert';

const ModalAddApointment = ({
  visible,
  visibleModalEditer,
  reWriteStateInTable,
  closeMove,
  current_id,
  updateAppointmentData,
}) => {
  const {openTable, saveData, updateDataFromId} = useSqlBuilder(); // name saveCard
  openTable('Appointments', appointmentScheme);
  const replaceDataCard = useReplaceCard();

  const [date, setDate] = useState(new Date());
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);
  const [updateDateOrTime, setUpdateDateOrTime] = useState(false); // false = date | true = time

  const [service, setService] = useState('');
  const [currentDate, setCurrentDate] = useState('');
  const [currentTime, setCurrentTime] = useState('');
  const [address, setAddress] = useState('');
  const [remember, setRemember] = useState('');

  const [isAddressEmpty, setIsAddressEmpty] = useState(false);
  const [isRememberEmpty, setIsRememberEmpty] = useState(false);
  const [isServiceEmpty, setIsServiceEmpty] = useState(false);

  const onChange = (event, selectedDate) => {
    const nowDate = selectedDate;
    setShow(false);
    setDate(nowDate);
  };

  function updateDate() {
    const days = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();
    const months =
      date.getMonth() < 10 ? `0${date.getMonth() + 1}` : date.getMonth();
    setCurrentDate(`${days}.${months}.${date.getFullYear()}`);
  }

  function updateTime() {
    const hours =
      date.getHours() < 10 ? `0${date.getHours()}` : date.getHours();
    const minutes =
      date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes();
    setCurrentTime(`${hours}:${minutes}`);
  }

  useEffect(() => {
    updateDateOrTime ? updateTime() : updateDate();
  }, [date]);

  useEffect(() => {
    updateTime();
    updateDate();
  }, []);

  const showMode = currentMode => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
    setUpdateDateOrTime(false);
  };

  const showTimepicker = () => {
    showMode('time');
    setUpdateDateOrTime(true);
  };

  const fieldsToCheck = [
    {value: address, setter: setIsAddressEmpty},
    {value: remember, setter: setIsRememberEmpty},
    {value: service, setter: setIsServiceEmpty},
  ];

  function resetFieldsInModal() {
    setAddress('');
    setRemember('');
    setService('');
    setIsAddressEmpty(false);
    setIsRememberEmpty(false);
    setIsServiceEmpty(false);
  }

  useEffect(() => {
    if (!visibleModalEditer) {
      resetFieldsInModal();
    }
    if (!visible) {
      resetFieldsInModal();
    }
  }, [visible, visibleModalEditer]);

  const reWriteNote = () => {
    if (reWriteStateInTable) {
      replaceDataCard(
        [current_id, service, currentDate, currentTime, address, remember],
        [updateAppointmentData, resetFieldsInModal],
      );
      rerenderComponent();
    }
  };

  const saveAppointments = () => {
    checkFieldsIsEmpty(fieldsToCheck);
    if (address.length > 0 && remember.length > 0) {
      saveData(
        [service, currentDate, currentTime, address, remember],
        appointmentFields,
        'Appointments',
        [updateAppointmentData, resetFieldsInModal],
      );
      rerenderComponent();
    }
  };

  // При нажатие на редактировать, мы получаем айди карточки и грузим данные в модальное окно из бд по айди
  useEffect(() => {
    updateDataFromId(
      'Appointments',
      current_id,
      [setService, setCurrentDate, setCurrentTime, setAddress, setRemember],
      appointmentFields,
    );
  }, [current_id]);

  return (
    <View style={styles.root}>
      <Modal animationType="fade" transparent={true} visible={visible}>
        <View style={styles.lowerItem}>
          <View style={styles.modalWindowBackPlaid}>
            <View style={styles.headerName}>
              <Text style={styles.boldText}>Услуга</Text>
            </View>

            <View style={styles.itemOne}>
              <TextInput
                style={[
                  styles.selectAppointmentItem,
                  {
                    borderWidth: 1,
                    borderColor: isServiceEmpty ? 'red' : 'transparent',
                  },
                ]}
                placeholderTextColor={INACTIVE_GREY}
                placeholder=""
                value={service}
                onChangeText={text => setService(text)}
              />
            </View>
            {show && (
              <DateTimePicker
                testID="dateTimePicker"
                value={date}
                mode={mode}
                is24Hour={true}
                onChange={(event, selectedDate) => {
                  if (event.type === 'set') {
                    onChange(event, selectedDate);
                    console.log('Дата изменена:', selectedDate);
                  } else if (event.type === 'dismissed') {
                    console.log('Пикер закрыт на кнопку отмены');
                    setShow(false);
                  }
                }}
              />
            )}
            <View style={styles.itemTwo}>
              <View style={{width: 120}}>
                <Text style={styles.textLight}>Дата</Text>
                <TouchableOpacity onPress={showDatepicker}>
                  <View style={styles.inputDate}>
                    <Text style={styles.textLight}>{currentDate}</Text>
                  </View>
                </TouchableOpacity>
              </View>

              <View
                style={{
                  width: 160,
                  marginLeft: 40,
                }}>
                <Text style={styles.textLight}>Время</Text>
                <TouchableOpacity onPress={showTimepicker}>
                  <View style={styles.inputTime}>
                    <Text style={styles.textLight}>{currentTime}</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>

            <View style={styles.itemThree}>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'column',
                }}>
                <Text style={styles.textLight}>Адрес</Text>
                <View style={styles.addresInputItemThree}>
                  <TextInput
                    style={[
                      styles.inputAddress,
                      {
                        borderWidth: 1,
                        borderColor: isAddressEmpty ? 'red' : 'transparent',
                      },
                    ]}
                    placeholderTextColor={INACTIVE_GREY}
                    placeholder=""
                    value={address}
                    onChangeText={text => setAddress(text)}
                  />
                </View>
              </View>
            </View>

            <View style={styles.itemFour}>
              <Text style={styles.textLight}>Напоминание</Text>
              <TextInput
                style={[
                  styles.inputRemember,
                  {
                    borderWidth: 1,
                    borderColor: isRememberEmpty ? 'red' : 'transparent',
                  },
                ]}
                placeholderTextColor={INACTIVE_GREY}
                placeholder=""
                value={remember}
                editable={true}
                onChangeText={text => setRemember(text)}
              />
            </View>

            <View style={styles.buttonsItem}>
              <Cbutton
                styleButton={styles.buttonCancel}
                colorButton={{backgroundColor: DARK_GREY}}
                name={'Отмена'}
                onPress={() => {
                  closeMove();
                  resetFieldsInModal();
                }}
              />
              <Cbutton
                styleButton={styles.buttonSave}
                colorButton={{backgroundColor: MAIN_COLOR}}
                name={'Сохранить'}
                onPress={() =>
                  reWriteStateInTable ? reWriteNote() : saveAppointments()
                }
              />
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  lowerItem: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
  },
  modalWindowBackPlaid: {
    //   justifyContent: 'center',
    //   alignItems: 'center',
    backgroundColor: '#e6e6e6',
    padding: 20,
    height: 360,
    width: 330,
    borderRadius: 22,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  headerName: {
    width: '100%',
    // borderWidth: 2,
  },
  boldText: {
    color: 'black',
    textAlign: 'left',
    fontWeight: 'bold',
  },
  textLight: {
    color: 'black',
  },
  itemOne: {
    // borderWidth: 2,
    width: '100%',
    marginTop: 1,
  },
  itemTwo: {
    width: '100%',
    // borderWidth: 2,
    // borderColor: 'red',
    marginTop: 1,
    flexDirection: 'row',
  },
  itemThree: {
    width: '100%',
    // borderWidth: 2,
    // borderColor: 'red',
    marginTop: 1,
    flexDirection: 'row',
  },
  addresInputItemThree: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
    // borderWidth: 2,
  },
  itemFour: {
    width: '100%',
    // borderWidth: 2,
    // borderColor: 'red',
    marginTop: 1,
    flexDirection: 'column',
  },
  selectAppointmentItem: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    height: 30,
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 7,
    // borderWidth: 1,
    margin: 3,
    paddingVertical: 3,
    paddingHorizontal: 10,
    // marginTop: 0,
  },
  inputDate: {
    color: 'black',
    justifyContent: 'center',
    alignItems: 'flex-start',
    height: 24,
    width: '90%',
    backgroundColor: 'white',
    borderRadius: 7,
    margin: 3,
    paddingVertical: 3,
    paddingHorizontal: 10,
  },
  inputTime: {
    color: 'black',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    height: 24,
    width: '60%',
    backgroundColor: 'white',
    borderRadius: 7,
    margin: 3,
    paddingVertical: 3,
    paddingHorizontal: 10,
  },
  inputAddress: {
    color: 'black',
    justifyContent: 'center',
    alignItems: 'flex-start',
    height: 30,
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 7,
    margin: 3,
    paddingVertical: 3,
    paddingHorizontal: 10,
  },
  inputRemember: {
    color: 'black',
    justifyContent: 'center',
    alignItems: 'center',
    height: 35,
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 7,
    margin: 3,
    paddingVertical: 3,
    paddingHorizontal: 10,
  },
  buttonsItem: {
    display: 'flex',
    // borderWidth: 2,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 5,
    left: 0,
    right: 0,
    flexDirection: 'row',
  },
  buttonCancel: {
    height: 38,
    width: 110,
    borderRadius: 20,
  },
  buttonSave: {
    height: 38,
    width: 110,
    borderRadius: 20,
  },
});

export default ModalAddApointment;
