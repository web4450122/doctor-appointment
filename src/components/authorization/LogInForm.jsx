import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import HorizontalSeparator from '../ui/HorizontalSeparator';
import Cbutton from '../ui/Cbutton';
import AlertModal from '../modalWindow/AlertModal';
import SvgButtons from '../ui/icons/SvgLoginPage/SvgButtons';
import SvgShowPass from '../ui/icons/SvgLoginPage/SvgShowPass';
import RegistringNewUser from './RegistringNewUser';
import {invalidLoginOrPass} from '../../assets/error/invalidData';
import {
  INACTIVE_GREY,
  MAIN_COLOR,
  RU_PHONE_DIGITS,
  PASSWORD_DIGITS,
} from '../../../App';
import {usePostRequest, useGetRequest} from '../../hooks/useApiHooks';
import {useHelperTableOperations} from '../../hooks/useRequestSqlHelper';
import {useSqlBuilder} from '../../hooks/useRequestBuilder';
import {scheme, fields} from '../../assets/tablesData/ActiveUserFields';

export let USER_ID = '';
export let TOKEN = '';

const LogInForm = () => {
  // 79771111111 12345678
  const [phone, setPhone] = useState('');
  const [password, setPassword] = useState('');
  const [isRememberMe, setIsRememberMe] = useState(0);

  let fullName = {
    firstName: '',
    lastName: '',
    surName: '',
  };

  const [registrationPhase, setRegistrationPhase] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [secureTextEntry, setSecureTextEntry] = useState(true);

  const postRequest = usePostRequest();
  const getRequest = useGetRequest();

  const {showTables, showTableContent, deleteTable, showShemeTable} =
    useHelperTableOperations();

  const {openTable, updateData, saveData} = useSqlBuilder();
  openTable('ActiveUser', scheme);

  const handlerSwitchPhaseRegistrationOne = () => {
    setRegistrationPhase(true);
  };

  const resetRegistrationPhase = () => {
    setRegistrationPhase(false);
  };

  const showPassword = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  useEffect(() => {
    updateData(
      [setPhone, setPassword, setIsRememberMe],
      ['phone', 'password', 'rememberMe'],
      'ActiveUser',
    );
  }, []);

  const navigation = useNavigation();

  async function logIn() {
    let deadCode = true;

    const result = await postRequest('login', {
      username: phone,
      password: password,
    });
    // К серверу доступа больше нет, этот блок более не действителен, вход осуществляется по хардкоду
    if (result.success) {
      USER_ID = result.data.user_id;
      TOKEN = 'Bearer ' + result.data.token;

      await getFullNameUser();
      navigation.navigate('MainPage');
      const userData = [
        phone,
        password,
        fullName.firstName,
        fullName.lastName,
        fullName.surName,
        USER_ID,
        TOKEN,
        1,
      ];
      if (!isRememberMe) {
        setIsRememberMe(1);
        saveData(userData, fields, 'ActiveUser');
        console.log('пользователь добавлен');
      }

      // -------------------------------------------------------------------------------------------
    } else if (deadCode) {
      const userData = [
        phone,
        password,
        'Петр',
        'Петров',
        'Петрович',
        '5434x123',
        'drgerere',
        1,
      ];
      navigation.navigate('MainPage');
      if (!isRememberMe) {
        setIsRememberMe(1);
        saveData(userData, fields, 'ActiveUser');
        console.log('пользователь добавлен');
      }
    } else {
      console.log(result.errorCode);
      invalidLoginOrPass('Неправильный логин или пароль');
    }
  }

  async function getFullNameUser() {
    const result = await getRequest(`userInfo/${USER_ID}`);
    if (result.success) {
      fullName.firstName = result.data.first_name;
      fullName.lastName = result.data.last_name;
      fullName.surName = result.data.surname;
    } else {
      console.log(result.errorCode);
    }
  }

  return (
    <>
      <RegistringNewUser
        visible={registrationPhase ? 'flex' : 'none'}
        cancelMove={resetRegistrationPhase}
      />
      <View
        style={[
          styles.logInFormWhiteItem,
          {display: registrationPhase ? 'none' : 'flex'},
        ]}>
        <View style={styles.titleItem}>
          <Text style={styles.boldText}>Вход</Text>
          <Text style={[styles.lightText, {color: INACTIVE_GREY}]}>
            Введите телефон и пароль для входа в систему
          </Text>
        </View>
        <HorizontalSeparator Styles={styles.separator} />
        <View style={styles.logInButtonsItem}>
          <View style={styles.svgButtonsInput}>
            <SvgButtons />
          </View>
          <TextInput
            style={styles.input}
            placeholderTextColor={INACTIVE_GREY}
            placeholder="Телефон"
            value={phone}
            onChangeText={text => setPhone(text)}
            keyboardType="phone-pad"
            maxLength={11}
          />
          <TextInput
            style={styles.input}
            placeholderTextColor={INACTIVE_GREY}
            placeholder="Пароль"
            value={password}
            onChangeText={text => setPassword(text)}
            secureTextEntry={secureTextEntry}
          />
          <TouchableOpacity
            style={styles.svgShowPassInput}
            onPress={showPassword}>
            <SvgShowPass />
          </TouchableOpacity>
          <HorizontalSeparator Styles={styles.separatorTwo} />
          <View style={styles.checkboxContainer}>
            <HorizontalSeparator Styles={styles.separatorThree} />
            <TouchableOpacity>
              <Text style={[styles.forgotPassword, {color: INACTIVE_GREY}]}>
                Забыл пароль?
              </Text>
            </TouchableOpacity>
          </View>
          <HorizontalSeparator Styles={styles.separatorFour} />
          <Cbutton
            styleButton={styles.buttonLogIn}
            colorButton={{
              backgroundColor: MAIN_COLOR,
            }}
            name={'Войти'}
            onPress={logIn}
            isDisabled={
              !(
                phone.length === RU_PHONE_DIGITS &&
                password.length >= PASSWORD_DIGITS
              )
            }
          />
          {/* <Cbutton
            styleButton={styles.buttonDebug}
            colorButton={{borderColor: MAIN_COLOR}}
            styleText={styles.buttonTextBlack}
            name={'показать содержимое'}
            onPress={() => showTableContent('ActiveUser')}
          />
          <Cbutton
            styleButton={styles.buttonDebug}
            colorButton={{borderColor: MAIN_COLOR}}
            styleText={styles.buttonTextBlack}
            name={'показать таблицы'}
            onPress={showTables}
          />
          <Cbutton
            styleButton={styles.buttonDebug}
            colorButton={{borderColor: MAIN_COLOR}}
            styleText={styles.buttonTextBlack}
            name={'показать схему таблицы'}
            onPress={() => showShemeTable('ActiveUser')}
          /> */}
          <Cbutton
            styleButton={styles.buttonRegistration}
            colorButton={{borderColor: MAIN_COLOR}}
            styleText={styles.buttonTextBlack}
            name={'Регистрация'}
            onPress={handlerSwitchPhaseRegistrationOne}
          />
          <TouchableOpacity onPress={() => setModalVisible(!modalVisible)}>
            <Text style={[styles.whitoutRegistration, {color: INACTIVE_GREY}]}>
              Войти без регистрации
            </Text>
          </TouchableOpacity>
          <AlertModal
            visible={modalVisible}
            closeMove={() => setModalVisible(!modalVisible)}
          />
        </View>
      </View>
    </>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
  boldText: {
    fontSize: 26,
    fontWeight: 'normal',
    color: 'black',
  },
  lightText: {
    textAlign: 'center',
  },
  forgotPassword: {
    fontSize: 14,
    fontFamily: 'Futura',
    textDecorationLine: 'underline',
  },
  whitoutRegistration: {
    fontSize: 14,
    fontFamily: 'Futura',
    textDecorationLine: 'underline',
    textAlign: 'center',
  },
  logInFormWhiteItem: {
    zIndex: 103,
    position: 'absolute',
    // display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    bottom: 0,
    borderTopRightRadius: 50,
    borderTopLeftRadius: 50,
    width: '100%',
    height: 450,
    backgroundColor: 'white',
  },
  titleItem: {
    // borderWidth: 3,
    width: '73%',
    alignItems: 'center',
    marginTop: '3%',
  },
  logInButtonsItem: {
    // display: 'flex',
    // flex: 1,
    // borderWidth: 3,
    borderColor: 'red',
    width: '73%',
    // marginBottom: '10%',
  },
  input: {
    height: 50,
    borderColor: 'gray',
    color: 'black',
    borderWidth: 1,
    borderRadius: 9,
    paddingHorizontal: 30,
    marginBottom: 10,
  },
  checkboxContainer: {
    display: 'flex',
    justifyContent: 'flex-end',
    flexDirection: 'row',
    alignItems: 'center',
  },
  svgButtonsInput: {
    position: 'absolute',
    top: 15,
    right: 25,
  },
  svgShowPassInput: {
    position: 'absolute',
    top: 74,
    right: 25,
  },
  separator: {
    height: 10,
    width: 1,
    backgroundColor: 'transparent',
  },
  separatorTwo: {
    height: 5,
    width: 1,
    backgroundColor: 'transparent',
  },
  separatorThree: {
    height: 1,
    width: 50,
    backgroundColor: 'transparent',
  },
  separatorFour: {
    height: 5,
    width: 1,
    backgroundColor: 'transparent',
  },
  buttonTextBlack: {
    color: 'black',
  },
  buttonLogIn: {
    height: 50,
    borderRadius: 20,
  },
  buttonRegistration: {
    height: 50,
    backgroundColor: 'transparent',
    borderRadius: 20,
    borderWidth: 2,
  },
  buttonDebug: {
    height: 50,
    backgroundColor: 'transparent',
    borderRadius: 20,
    borderWidth: 2,
  },
});

export default LogInForm;
