/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, {useState, useCallback} from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  TextInput,
  Modal,
  TouchableOpacity,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import HorizontalSeparator from '../ui/HorizontalSeparator';
import Cbutton from '../ui/Cbutton';
import AlertModal from '../modalWindow/AlertModal';
import SvgButtons from '../ui/icons/SvgLoginPage/SvgButtons';
import SvgShowPass from '../ui/icons/SvgLoginPage/SvgShowPass';
import {
  MAIN_COLOR,
  RU_PHONE_DIGITS,
  SMS_DIGITS,
  PASSWORD_DIGITS,
  INACTIVE_GREY,
} from '../../../App';

const dataText = {
  phase1: {
    lowerText:
      'Введите телефон, указанный в договоре с Центром\nэпилептологии и неврологии',
  },
  phase2: {
    lowerText: 'Введите код подтверждения из СМС',
  },
  phase3: {
    lowerText: 'Придумайте пароль, минимум 8 символов',
  },
};

const RegistringNewUser = ({visible, cancelMove}) => {
  const [registrationPhase, setRegistrationPhase] = useState(1);
  const [phoneAndSms, setPhoneAndSms] = useState('79771111111');
  const [password, setPassword] = useState('');
  const [repeatPassword, setRepeatPassword] = useState('');
  const [isPasswordVisible, setIsPasswordVisible] = useState(true);

  const returnTextPhase = useCallback(() => {
    return dataText[`phase${registrationPhase}`]?.lowerText || '';
  }, [registrationPhase]);

  const sendPhone = () => {
    console.log(phoneAndSms, 'Номер телефона улетел на сервер');
    setPhoneAndSms('');
    setRegistrationPhase(2);
  };

  const sendCodeAccepting = () => {
    console.log(phoneAndSms, 'Код подтверждения улетел на сервер');
    setPhoneAndSms('');
    setRegistrationPhase(3);
  };

  const sendPassword = () => {
    console.log(password, 'Пароль улетел на сервер');
    setPassword('');
    setRepeatPassword('');
    exit();
  };

  const exit = () => {
    setRegistrationPhase(1);
    setPhoneAndSms('');
    setPassword('');
    setRepeatPassword('');
    cancelMove();
  };

  const togglePasswordVisibility = () => {
    setIsPasswordVisible(!isPasswordVisible);
  };

  return (
    <View style={[styles.logInFormWhiteItem, {display: visible}]}>
      <View style={styles.titleItem}>
        <Text style={styles.boldText}>Регистрация нового пользователя</Text>
        <Text style={[styles.lightText, {color: INACTIVE_GREY}]}>
          {returnTextPhase()}
        </Text>
      </View>
      <HorizontalSeparator Styles={styles.separator} />
      <View style={styles.logInButtonsItem}>
        {registrationPhase !== 3 && (
          <TextInput
            style={styles.input}
            placeholderTextColor={INACTIVE_GREY}
            placeholder={
              registrationPhase === 1 ? 'Телефон' : 'Код подтверждения'
            }
            value={phoneAndSms}
            onChangeText={text => setPhoneAndSms(text)}
            keyboardType="phone-pad"
          />
        )}
        {registrationPhase === 3 && (
          <>
            <TextInput
              style={styles.input}
              placeholder="Пароль"
              value={password}
              onChangeText={text => setPassword(text)}
              secureTextEntry={isPasswordVisible}
            />
            <TextInput
              style={styles.input}
              placeholder="Повторите пароль"
              value={repeatPassword}
              onChangeText={text => setRepeatPassword(text)}
              secureTextEntry={isPasswordVisible}
            />
            <View style={styles.svgButtonsInput}>
              <TouchableOpacity onPress={togglePasswordVisibility}>
                <SvgShowPass />
              </TouchableOpacity>
            </View>
          </>
        )}

        <HorizontalSeparator Styles={styles.separatorTwo} />
        <Cbutton
          styleButton={styles.buttonSend}
          colorButton={{backgroundColor: MAIN_COLOR}}
          name={'Отправить'}
          onPress={() =>
            registrationPhase === 1
              ? sendPhone()
              : registrationPhase === 2
              ? sendCodeAccepting()
              : registrationPhase === 3
              ? sendPassword()
              : null
          }
          isDisabled={
            registrationPhase === 1
              ? !(phoneAndSms.length === RU_PHONE_DIGITS)
              : registrationPhase === 2
              ? !(phoneAndSms.length === SMS_DIGITS)
              : !(
                  password.length >= PASSWORD_DIGITS &&
                  repeatPassword.length >= PASSWORD_DIGITS &&
                  password === repeatPassword
                )
          }
        />
        <HorizontalSeparator Styles={styles.separatorThree} />
        <Cbutton
          styleButton={styles.buttonCancel}
          colorButton={{borderColor: MAIN_COLOR}}
          styleText={styles.buttonTextBlack}
          name={'Отмена'}
          onPress={exit}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
  titleItem: {
    width: '73%',
    alignItems: 'center',
    marginTop: '3%',
  },
  boldText: {
    fontSize: 18,
    fontWeight: 'normal',
    color: 'black',
  },
  lightText: {
    textAlign: 'center',
    marginVertical: 20,
  },
  logInFormWhiteItem: {
    zIndex: 102,
    position: 'absolute',
    // display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    bottom: 0,
    borderTopRightRadius: 50,
    borderTopLeftRadius: 50,
    width: '100%',
    height: 450,
    backgroundColor: 'white',
  },
  logInButtonsItem: {
    // display: 'flex',
    // flex: 1,
    // borderWidth: 3,
    borderColor: 'red',
    width: '73%',
    // marginBottom: '10%',
  },
  input: {
    color: 'black',
    height: 50,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 9,
    paddingHorizontal: 30,
    marginBottom: 10,
  },
  svgButtonsInput: {
    position: 'absolute',
    top: 15,
    right: 25,
  },
  separator: {
    height: 10,
    width: 0,
    backgroundColor: 'transparent',
  },
  separatorTwo: {
    height: 1,
    width: 1,
    backgroundColor: 'transparent',
  },
  separatorThree: {
    height: 10,
    width: 1,
    backgroundColor: 'transparent',
  },
  buttonSend: {
    height: 50,
    borderRadius: 20,
  },
  buttonCancel: {
    height: 50,
    backgroundColor: 'transparent',
    borderRadius: 20,
    borderWidth: 2,
  },
  buttonTextBlack: {
    color: 'black',
  },
});

export default RegistringNewUser;
