/* eslint-disable prettier/prettier */
import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';

//
//
// ВЕСЬ КОД С ЧАТОМ НА ДАННЫЙ МОМЕНТ ЗАГЛУШКА И НЕ БОЛЕЕ
//
//

const MessageItem = ({message, img}) => {
  return (
    <React.Fragment>
      <View style={styles.messageContainer}>
        <Text style={styles.messageText}>{message}</Text>
        {img ? (
          <Image
            source={{uri: img}}
            style={{width: 260, height: 200}}
            resizeMode="cover"
          />
        ) : null}
      </View>
    </React.Fragment>
  );
};
// TODO добавить иконку, закрыть добавление фотографии
const styles = StyleSheet.create({
  messageContainer: {
    minHeight: 35,
    maxWidth: '70%',
    marginRight: 15,
    marginBottom: 8,
    flexDirection: 'row',
    flexWrap: 'wrap',
    backgroundColor: '#087B6B',
    borderRadius: 8,
  },
  messageText: {
    textAlign: 'left',
    color: 'white',
    marginTop: 2,
    padding: 8,
  },
});

export default MessageItem;
