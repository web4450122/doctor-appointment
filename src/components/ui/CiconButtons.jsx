/* eslint-disable prettier/prettier */
import React from 'react';
import {TouchableOpacity} from 'react-native';

const CiconButtons = ({icon, onPress, visibility}) => {
  return (
    <TouchableOpacity style={{display: visibility}} onPress={onPress}>
      {icon}
    </TouchableOpacity>
  );
};

export default CiconButtons;

// require('./assets/logo.png')
// {width: 100, height: 140}
// "contain"
