import React from 'react';
import {View} from 'react-native';

const VerticalSeparator = ({_height, _backgroundColor, _width}) => {
  return (
    <View
      style={{
        height: _height,
        backgroundColor: _backgroundColor,
        width: _width,
      }}
    />
  );
};

export default VerticalSeparator;
