/* eslint-disable prettier/prettier */
import React, {useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import {useRoute} from '@react-navigation/native';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import {MAIN_COLOR} from '../../../App';
import CiconButtons from './CiconButtons';
import SvgAttack from './icons/SvgFooter/SvgAttack';
import SvgChat from './icons/SvgFooter/SvgChat';
import SvgConclusion from './icons/SvgFooter/SvgConclusion';
import SvgMedicament from './icons/SvgFooter/SvgMedicament';
import SvgRecords from './icons/SvgFooter/SvgRecords';

const Footer = () => {
  const navigation = useNavigation();
  const route = useRoute();
  const currentScreen = route.name;
  return (
    <>
      <View style={styles.container}>
        <CiconButtons
          icon={
            <SvgAttack
              _color={currentScreen === 'SeizuresPage' ? MAIN_COLOR : '#fff'}
            />
          }
          onPress={() => navigation.navigate('SeizuresPage')}
        />
        <CiconButtons
          icon={
            <SvgChat
              _color={currentScreen === 'MessagesPage' ? MAIN_COLOR : '#fff'}
            />
          }
          onPress={() => navigation.navigate('MessagesPage')}
        />
        <CiconButtons icon={<SvgConclusion />} />
        <CiconButtons
          icon={
            <SvgMedicament
              _color={currentScreen === 'MedicinePage' ? MAIN_COLOR : '#fff'}
            />
          }
          onPress={() => navigation.navigate('MedicinePage')}
        />
        <CiconButtons
          icon={
            <SvgRecords
              _color={
                currentScreen === 'AppointmentsPage' ? MAIN_COLOR : '#fff'
              }
            />
          }
          onPress={() => navigation.navigate('AppointmentsPage')}
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    flexDirection: 'row',
    position: 'absolute',
    bottom: 0,
    height: 70,
    marginBottom: 12,
    width: '95%',
    backgroundColor: '#454F55',
    shadowColor: '#000',
    borderRadius: 32,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 10,
  },
});

export default Footer;
