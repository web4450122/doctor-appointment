/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React from 'react';
import {View, StyleSheet} from 'react-native';

const HorizontalSeparator = ({Styles, Color}) => {
  return (
    <View
      style={{
        ...styles.styles,
        ...Styles,
        ...Color,
      }}
    />
  );
};

const styles = StyleSheet.create({
  styles: {
    height: 1,
    width: 1,
    marginLeft: 0,
    backgroundColor: 'grey',
  },
});

export default HorizontalSeparator;
