/* eslint-disable prettier/prettier */
import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';

const CheckBox = ({label, checked, onPress}) => {
  return (
    <TouchableOpacity style={styles.checkboxContainer} onPress={onPress}>
      <View style={styles.checkbox}>
        <View
          style={[styles.checkbox, checked ? styles.checked : styles.unchecked]}
        />
      </View>
      <Text style={styles.text}>{label}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  checkboxContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
  },
  checkbox: {
    width: 19,
    height: 19,
    borderRadius: 12,
    borderWidth: 2,
    borderColor: '#18D6B8',
    marginRight: 10,
  },
  checked: {
    height: 14,
    width: 14,
    backgroundColor: 'grey',
  },
  unchecked: {
    backgroundColor: 'transparent',
  },
  text: {
    color: 'black',
  },
});

export default CheckBox;
