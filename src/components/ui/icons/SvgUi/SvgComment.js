/* eslint-disable prettier/prettier */
import * as React from 'react';
import Svg, {Path} from 'react-native-svg';
const SvgComment = () => (
  <Svg fill="none" height={23} width={21}>
    <Path
      fill="#fff"
      d="M18 2.75h2.5v20H4.25A3.745 3.745 0 0 1 .5 19V4A3.745 3.745 0 0 1 4.25.25H15.5v17.5H4.25C3.562 17.75 3 18.313 3 19c0 .688.563 1.25 1.25 1.25H18V2.75Z"
    />
  </Svg>
);
export default SvgComment;
