/* eslint-disable prettier/prettier */
import * as React from 'react';
import Svg, {G, Path, Defs, ClipPath} from 'react-native-svg';
const SvgArrowR = () => (
  <Svg fill="none" height={20} width={20}>
    <G clipPath="url(#a)">
      <Path
        stroke="#454F55"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
        d="m4.48 1.94 7.454 7.2-7.454 7.2"
      />
    </G>
    <Defs>
      <ClipPath id="a">
        <Path fill="#fff" d="M16 .5H0v18h16z" />
      </ClipPath>
    </Defs>
  </Svg>
);
export default SvgArrowR;
