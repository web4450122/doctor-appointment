import * as React from 'react';
import Svg, {Path} from 'react-native-svg';
const SvgBackArrow = () => (
  <Svg fill="none" height={24} width={30}>
    <Path
      stroke="#18D6B8"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={3}
      d="M17 2 6 12l11 10"
    />
  </Svg>
);
export default SvgBackArrow;
