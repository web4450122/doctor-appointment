import * as React from 'react';
import Svg, {Path} from 'react-native-svg';
const SvgFile = () => (
  <Svg fill="none" height={30} width={30}>
    <Path
      fill="#fff"
      d="M10.736 8.55h6.188l-6.188-6.325V8.55ZM2.861.5h9l6.75 6.9v13.8c0 .61-.237 1.195-.659 1.626a2.225 2.225 0 0 1-1.59.674h-13.5a2.225 2.225 0 0 1-1.592-.674A2.326 2.326 0 0 1 .611 21.2V2.8c0-1.276 1.001-2.3 2.25-2.3Zm1.125 20.7h2.25v-6.9h-2.25v6.9Zm4.5 0h2.25V12h-2.25v9.2Zm4.5 0h2.25v-4.6h-2.25v4.6Z"
    />
  </Svg>
);
export default SvgFile;
