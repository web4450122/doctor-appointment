import * as React from 'react';
import Svg, {Path} from 'react-native-svg';
const SvgInfo = () => (
  <Svg fill="none" height={30} width={30}>
    <Path
      fill="#fff"
      d="M12.611 2c-5.52 0-10 4.48-10 10s4.48 10 10 10 10-4.48 10-10-4.48-10-10-10Zm0 15c-.55 0-1-.45-1-1v-4c0-.55.45-1 1-1s1 .45 1 1v4c0 .55-.45 1-1 1Zm1-8h-2V7h2v2Z"
    />
  </Svg>
);
export default SvgInfo;
