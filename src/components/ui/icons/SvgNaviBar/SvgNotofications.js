import * as React from 'react';
import Svg, {Path} from 'react-native-svg';
const SvgNotofications = () => (
  <Svg fill="none" height={30} width={30}>
    <Path
      fill="#fff"
      d="M13.111 23.938a3.911 3.911 0 0 0 3.58-2.344H9.531a3.911 3.911 0 0 0 3.58 2.343Zm7.031-9.375v-2.956c0-3.441-1.336-6.439-4.687-7.2l-.39-2.345h-3.907l-.39 2.344c-3.364.762-4.688 3.748-4.688 7.2v2.957l-2.344 3.124v2.344h18.75v-2.343l-2.344-3.125Z"
    />
  </Svg>
);
export default SvgNotofications;
