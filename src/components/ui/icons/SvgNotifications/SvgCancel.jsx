/* eslint-disable prettier/prettier */
import * as React from 'react';
import Svg, {Rect, Path} from 'react-native-svg';
const SvgCancel = () => (
  <Svg fill="none" height={41} width={40}>
    <Rect
      width={39}
      height={38}
      x={1}
      y={1.5}
      stroke="#F54148"
      strokeWidth={2}
      rx={19}
    />
    <Path
      stroke="#F54148"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={1.5}
      d="M26.39 14.61 14.61 26.39m0-11.78 11.78 11.78"
    />
  </Svg>
);
export default SvgCancel;
