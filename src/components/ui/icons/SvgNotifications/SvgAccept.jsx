/* eslint-disable prettier/prettier */
import * as React from 'react';
import Svg, {Rect, Path} from 'react-native-svg';
const SvgAccept = () => (
  <Svg fill="none" height={41} width={40}>
    <Rect
      width={38}
      height={38}
      x={1}
      y={1.5}
      stroke="#18D6B8"
      strokeWidth={2}
      rx={19}
    />
    <Path
      stroke="#18D6B8"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={1.5}
      d="m11.797 21.672 5.469 5.469 10.937-11.72"
    />
  </Svg>
);
export default SvgAccept;
