import * as React from 'react';
import Svg, {Path} from 'react-native-svg';
const SvgSend = () => (
  <Svg fill="none" height={30} width={30}>
    <Path
      fill="#087B6B"
      d="M2.694 1.645a.781.781 0 0 0-1.116.852l2.192 7.578a.78.78 0 0 0 .62.553l8.891 1.49c.419.082.419.682 0 .765l-8.89 1.489a.781.781 0 0 0-.62.553l-2.193 7.578a.781.781 0 0 0 1.116.852l20.312-10.157a.781.781 0 0 0 0-1.396L2.694 1.645Z"
    />
  </Svg>
);
export default SvgSend;
