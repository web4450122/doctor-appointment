import * as React from 'react';
import Svg, {G, Path, Defs, ClipPath} from 'react-native-svg';
const SvgSearch = () => (
  <Svg fill="none" height={27} width={26}>
    <G clipPath="url(#a)">
      <Path
        fill="#6A6A6D"
        d="m24.225 21.775-5.922-5.036c-.612-.552-1.267-.803-1.795-.78a9.374 9.374 0 1 0-1.05 1.05c-.025.529.228 1.183.78 1.796l5.035 5.922c.863.957 2.27 1.039 3.13.18.86-.86.778-2.27-.18-3.13l.002-.002Zm-14.85-5.65a6.25 6.25 0 1 1 0-12.5 6.25 6.25 0 0 1 0 12.5Z"
      />
    </G>
    <Defs>
      <ClipPath id="a">
        <Path fill="#fff" d="M0 .5h25v25H0z" />
      </ClipPath>
    </Defs>
  </Svg>
);
export default SvgSearch;
