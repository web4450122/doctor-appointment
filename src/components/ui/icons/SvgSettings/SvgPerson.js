/* eslint-disable prettier/prettier */
import * as React from 'react';
import Svg, {Path} from 'react-native-svg';
const SvgPerson = () => (
  <Svg fill="none" height={17} width={17}>
    <Path
      fill="#18D6B8"
      d="M8.5.167a4.167 4.167 0 1 1 0 8.333 4.167 4.167 0 0 1 0-8.333Zm0 10.416c4.604 0 8.333 1.865 8.333 4.167v2.083H.167V14.75c0-2.302 3.729-4.167 8.333-4.167Z"
    />
  </Svg>
);
export default SvgPerson;
