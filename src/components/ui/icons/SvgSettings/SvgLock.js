/* eslint-disable prettier/prettier */
import * as React from 'react';
import Svg, {Path} from 'react-native-svg';
const SvgLock = ({scale = 1}) => (
  <Svg fill="none" height={24 * scale} width={22 * scale}>
    <Path
      fill="#18D6B8"
      fillRule="evenodd"
      d="M.125 10.5A3.125 3.125 0 0 1 3.25 7.375h12.5a3.125 3.125 0 0 1 3.125 3.125v7.292a3.125 3.125 0 0 1-3.125 3.125H3.25a3.125 3.125 0 0 1-3.125-3.125V10.5Zm10.417 2.083a1.042 1.042 0 0 0-2.084 0v3.125a1.042 1.042 0 0 0 2.084 0v-3.125Z"
      clipRule="evenodd"
      transform={{scale}}
    />
    <Path
      stroke="#18D6B8"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={2}
      d="M5.333 8.417V5.292a4.167 4.167 0 0 1 8.334 0v3.125"
      transform={{scale}}
    />
  </Svg>
);
export default SvgLock;
