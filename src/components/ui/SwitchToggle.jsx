/* eslint-disable prettier/prettier */
import React, {useState, useEffect} from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';
import {MAIN_COLOR} from '../../../App';
import {INACTIVE_GREY} from '../../../App';
import {ACTIVE_GREEN} from '../../../App';

const SwitchToggle = ({isLeft, setIsLeft}) => {
  // const [isLeft, setIsLeft] = useState(1);

  const toggleSwitch = () => {
    setIsLeft(!isLeft);
  };
  useEffect(() => {
    console.log(isLeft);
  }, []);

  return (
    <TouchableOpacity onPress={toggleSwitch}>
      <View
        style={[
          isLeft
            ? {...styles.switchContainerLeft, backgroundColor: INACTIVE_GREY}
            : {...styles.switchContainerRight, backgroundColor: ACTIVE_GREEN},
          styles.root,
        ]}>
        <View
          style={[
            isLeft
              ? {...styles.switchButtonLeft}
              : {...styles.switchButtonRight, backgroundColor: MAIN_COLOR},
            isLeft ? styles.switchLeft : styles.switchRight,
          ]}
        />
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  root: {
    marginTop: 8,
  },
  switchContainerLeft: {
    width: 40,
    height: 20,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: '#ccc',
    overflow: 'hidden',
  },
  switchContainerRight: {
    width: 40,
    height: 20,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: '#ccc',
    overflow: 'hidden',
  },
  switchButtonLeft: {
    width: 20,
    borderRadius: 14,
    height: '100%',
    backgroundColor: 'white',
    position: 'absolute',
  },
  switchButtonRight: {
    width: 20,
    borderRadius: 14,
    height: '100%',
    position: 'absolute',
  },
  switchLeft: {
    left: 0,
  },
  switchRight: {
    right: 0,
  },
});

export default SwitchToggle;
