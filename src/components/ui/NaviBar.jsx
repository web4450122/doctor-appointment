import React from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {useRoute} from '@react-navigation/native';
import CiconButtons from './CiconButtons';
import SvgInfo from './icons/SvgNaviBar/SvgInfo';
import SvgNotofications from './icons/SvgNaviBar/SvgNotofications';
import SvgCalendar from './icons/SvgNaviBar/SvgCalendar';
import SvgFile from './icons/SvgNaviBar/SvgFile';
import SvgSettings from './icons/SvgNaviBar/SvgSettings';
import SvgBackArrow from './icons/SvgUi/SvgBackArrow';

const NaviBar = ({namePage}) => {
  const navigation = useNavigation();
  const route = useRoute();
  const currentScreen = route.name;

  const displayLogoNaviBar = () => {
    if (currentScreen === 'MainPage') {
      return (
        <View style={styles.logoAndTextNaviBarItem}>
          <Image
            source={require('../../assets/image/logo.png')}
            style={styles.logoImg}
            resizeMode="contain"
          />
          <Text style={styles.headerNameCurrentScreen}>
            ЦЕНТР МРТ И{'\n'}НЕВРОЛОГИИ. ИМ.{'\n'}Г.С. ХАНЗО
          </Text>
        </View>
      );
    } else {
      return (
        <View style={styles.logoAndTextNaviBarItem}>
          <CiconButtons
            icon={<SvgBackArrow />}
            onPress={
              currentScreen === 'MessagesObjectPage' ||
              currentScreen === 'SettingsUserInfo' ||
              currentScreen === 'SettingsPassword'
                ? () => {
                    navigation.goBack();
                    console.log(currentScreen);
                  }
                : () => {
                    navigation.navigate('MainPage');
                  }
            }
          />
          <Text style={styles.headerNameAppText}>{namePage}</Text>
        </View>
      );
    }
  };

  return (
    <>
      <View
        style={[
          styles.navibar,
          currentScreen === 'MessagesObjectPage'
            ? {justifyContent: 'flex-start'}
            : {justifyContent: 'space-around'},
        ]}>
        {displayLogoNaviBar()}
        <View
          style={[
            styles.iconsItem,
            {
              display:
                currentScreen === 'MessagesObjectPage' ? 'none' : 'block',
            },
            {marginLeft: currentScreen === 'MainPage' ? 10 : 100},
          ]}>
          {currentScreen === 'MainPage' && (
            <CiconButtons
              icon={<SvgInfo />}
              onPress={() => navigation.navigate('AboutPage')}
            />
          )}
          <CiconButtons
            icon={<SvgNotofications />}
            onPress={() => navigation.navigate('NotificationsPage')}
          />
          <CiconButtons
            icon={<SvgCalendar />}
            onPress={() => navigation.navigate('DataPicker')}
          />
          <CiconButtons icon={<SvgFile />} onPress={() => {}} />
          <CiconButtons
            icon={<SvgSettings />}
            onPress={() => navigation.navigate('Settings')}
          />
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  navibar: {
    display: 'flex',
    justifyContent: 'space-around', // flex-start
    alignItems: 'center',
    flexDirection: 'row',
    height: 45,
    width: '100%',
    paddingHorizontal: 12,
    backgroundColor: '#454F55',
  },
  logoAndTextNaviBarItem: {
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
    height: 45,
  },
  logoImg: {
    width: 40,
    height: 50,
  },
  headerNameAppText: {
    color: 'white',
    textShadowRadius: 8,
  },
  headerNameCurrentScreen: {
    fontSize: 10,
    color: 'white',
    textShadowRadius: 8,
  },
  iconsItem: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
});

export default NaviBar;
