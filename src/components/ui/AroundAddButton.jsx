/* eslint-disable react/self-closing-comp */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {MAIN_COLOR} from '../../../App';

const AroundAddButton = ({onPress}) => {
  return (
    <>
      <TouchableOpacity
        style={[styles.itemButton, {backgroundColor: MAIN_COLOR}]}
        onPress={onPress}>
        <Text style={styles.text}>+</Text>
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  itemButton: {
    height: 60,
    width: 60,
    borderRadius: 30,
    position: 'absolute',
    bottom: 10,
  },
  text: {
    color: 'white',
    fontSize: 40,
    textAlign: 'center',
  },
});

export default AroundAddButton;
