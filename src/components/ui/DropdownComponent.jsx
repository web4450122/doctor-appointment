/* eslint-disable prettier/prettier */
import React, {useState} from 'react';
import {View, Picker, StyleSheet} from 'react-native';

const DropdownComponent = () => {
  const [selectedValue, setSelectedValue] = useState('option1'); // Изначально выбранный элемент

  return (
    <View style={styles.container}>
      <Picker
        selectedValue={selectedValue}
        style={styles.picker}
        onValueChange={itemValue => setSelectedValue(itemValue)}>
        <Picker.Item label="Option 1" value="option1" />
        <Picker.Item label="Option 2" value="option2" />
        <Picker.Item label="Option 3" value="option3" />
      </Picker>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  picker: {
    width: 200,
    height: 50,
    backgroundColor: '#e0e0e0',
  },
});

export default DropdownComponent;
