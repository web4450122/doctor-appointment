/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import HorizontalSeparator from '../ui/HorizontalSeparator';
import {MAIN_COLOR, INACTIVE_GREY} from '../../../App';
import {useUpdateDataSupplemented} from '../../hooks/useRequestBuilder';
import {useCreateOrOpenTable} from '../../hooks/useRequestBuilder';
import {seizureFields} from '../../assets/tablesData/SeizuresFields';
import {getMinutesText} from '../../assets/pluralization/Pluralization';
import {render} from '../../assets/render/RerenderComponent';

const MainPageAttackCard = () => {
  const [seizuresData, setSeizuresData] = useState([]);
  const modifiedSeizuresData = seizuresData.map(item => ({
    ...item,
    seizure: removeTextAfterOpeningBracket(item.seizure),
  }));

  function removeTextAfterOpeningBracket(text) {
    const openingBracketIndex = text.indexOf('(');
    if (openingBracketIndex !== -1) {
      return text.substring(0, openingBracketIndex);
    }
    return text;
  }

  const updateSeizuresData = useUpdateDataSupplemented();
  const createOrOpenTable = useCreateOrOpenTable();
  createOrOpenTable('Seizures', seizureFields);

  useEffect(() => {
    updateSeizuresData(
      'Seizures',
      {
        id: 'id',
        seizure: 'seizure',
        currentTime: 'currentTime',
        continueMinutes: 'continueMinutes',
      },
      setSeizuresData,
    );
  }, [render]);

  function checkHaveAttacks() {
    if (modifiedSeizuresData.length) {
      return (
        <React.Fragment>
          {modifiedSeizuresData.map((appointment, index) => (
            <React.Fragment key={index}>
              <View style={styles.cardItem}>
                <View style={styles.cubeTime}>
                  <Text style={styles.textTime}>{appointment.currentTime}</Text>
                </View>
                <View style={{marginLeft: 10}}>
                  <Text style={styles.text}>{appointment.seizure}</Text>
                  <Text style={styles.text}>
                    {appointment.continueMinutes}{' '}
                    {getMinutesText(appointment.continueMinutes)}
                  </Text>
                </View>
              </View>
              {index !== modifiedSeizuresData.length - 1 && (
                <HorizontalSeparator Styles={styles.separator} />
              )}
            </React.Fragment>
          ))}
        </React.Fragment>
      );
    } else {
      return (
        <React.Fragment>
          <View style={styles.cardItem}>
            <Text style={{color: INACTIVE_GREY}}>Пусто</Text>
          </View>
        </React.Fragment>
      );
    }
  }

  return (
    <>
      <View style={styles.constainer}>
        <View style={styles.titleItem}>
          <Text style={styles.title}>ПРИСТУПЫ</Text>
        </View>
        <HorizontalSeparator
          Styles={styles.separator}
          Color={{backgroundColor: MAIN_COLOR}}
        />
        {checkHaveAttacks()}
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  constainer: {
    display: 'flex',
    // justifyContent: 'center',
    alignItems: 'center',
    // height: 110,
    width: '85%',
    marginTop: 10,
    // borderWidth: 2,
    backgroundColor: 'white',
    borderRadius: 18,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 3,
  },
  cardItem: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
    height: 55,
    width: '85%',
    // borderWidth: 2,
    // borderColor: 'pink',
    marginTop: 2,
  },
  cubeTime: {
    display: 'flex',
    justifyContent: 'center',
    height: 30,
    width: 45,
    backgroundColor: '#18D6B8',
    borderRadius: 14,
  },
  titleItem: {
    height: 40,
    width: '85%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-start',
    // borderWidth: 2,
    // borderColor: 'pink',
  },
  title: {
    marginTop: 2,
    fontSize: 19,
    color: 'black',
  },
  text: {
    color: 'black',
  },
  textTime: {
    textAlign: 'center',
    color: 'white',
  },
  separator: {
    height: 1,
    width: '85%',
  },
});

export default MainPageAttackCard;
