/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import HorizontalSeparator from '../ui/HorizontalSeparator';
import ModalReminder from '../modalWindow/ModalReminder';
import {MAIN_COLOR, INACTIVE_GREY} from '../../../App';
import {useUpdateDataSupplemented} from '../../hooks/useRequestBuilder';
import {useCreateOrOpenTable} from '../../hooks/useRequestBuilder';
import {MedicamentsFields} from '../../assets/tablesData/MedicamentsFields';
import {render} from '../../assets/render/RerenderComponent';

export const dataTime = [
  {
    time: 'Утро',
  },
  {
    time: 'День',
  },
  {
    time: 'Вечер',
  },
];

const MainPageReminderMedicaments = () => {
  const [medData, setMedData] = useState([]);

  const [visible, setVisible] = useState(false);

  const closeModal = () => {
    setVisible(false);
  };

  const openModal = () => {
    setVisible(true);
  };

  const updateMedicamentsData = useUpdateDataSupplemented();
  const createOrOpenTable = useCreateOrOpenTable();
  createOrOpenTable('Medicaments', MedicamentsFields);

  useEffect(() => {
    updateMedicamentsData(
      'Medicaments',
      {id: 'id', preparat: 'preparat', dose: 'dose'},
      setMedData,
    );
  }, [render]);

  function checkHaveMedicaments() {
    if (medData.length) {
      return (
        <React.Fragment>
          <View style={styles.itemsContainer}>
            {dataTime.map((item, index) => (
              <React.Fragment key={index}>
                <View style={styles.cardItem}>
                  <View style={styles.timeContainer}>
                    <Text style={styles.textLight}>{item.time}</Text>
                  </View>
                  <View style={styles.medicamentContainer}>
                    {medData.map((medicament, innerIndex) => (
                      <React.Fragment key={innerIndex}>
                        <View style={styles.medicamentItem}>
                          <Text style={styles.medicamentText}>
                            {medicament.preparat}
                          </Text>
                          <Text
                            style={[styles.medicamentText, {marginLeft: 40}]}>
                            {medicament.dose}
                          </Text>
                          <Text style={[styles.medicamentText]}>
                            {medicament.ready}
                          </Text>
                        </View>
                      </React.Fragment>
                    ))}
                  </View>
                </View>
                {index !== dataTime.length - 1 && (
                  <HorizontalSeparator Styles={styles.separator} />
                )}
              </React.Fragment>
            ))}
          </View>
        </React.Fragment>
      );
    } else {
      return (
        <React.Fragment>
          <View style={styles.voidItem}>
            <Text style={{color: INACTIVE_GREY}}>Пусто</Text>
          </View>
        </React.Fragment>
      );
    }
  }

  return (
    <View style={styles.container}>
      <View style={styles.itemTitle}>
        <Text style={styles.title}>ПРИЕМ ЛЕКАРСТВ</Text>
        <TouchableOpacity style={styles.pointsButton} onPress={openModal}>
          <Text style={[styles.pointsText, {color: MAIN_COLOR}]}>...</Text>
        </TouchableOpacity>
      </View>
      <HorizontalSeparator
        Styles={styles.separatorMargin}
        Color={{backgroundColor: MAIN_COLOR}}
      />
      {checkHaveMedicaments()}
      <ModalReminder visible={visible} cancelMove={closeModal} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    width: '85%',
    marginTop: 10,
    backgroundColor: 'white',
    borderRadius: 18,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 3,
  },
  itemTitle: {
    height: 40,
    width: '85%',
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'row',
    marginLeft: 24,
    paddingTop: 4,
    // borderWidth: 2,
    // borderColor: 'pink',
  },
  title: {
    marginTop: 2,
    fontSize: 19,
    color: 'black',
    // paddingHorizontal: 30,
    // paddingTop: 5,
  },
  pointsButton: {
    marginLeft: 100,
  },
  pointsText: {
    fontSize: 22,
    fontWeight: 'bold',
  },
  itemsContainer: {
    paddingHorizontal: 24,
    // borderWidth: 2,
    // borderColor: 'gold',
  },
  cardItem: {
    flexDirection: 'row',
    marginTop: 5,
    // marginLeft: 14,
    // borderWidth: 2,
  },
  voidItem: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
    height: 55,
    width: '85%',
    // borderWidth: 2,
    // borderColor: 'pink',
    marginTop: 2,
    marginLeft: 22,
  },
  timeContainer: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    marginRight: 30,
    height: 35,
    width: 40,
    backgroundColor: 'transparent', // #F9F9F9
  },
  medicamentContainer: {
    flex: 1,
    flexDirection: 'column',
    // borderWidth: 2,
    // borderColor: 'red',
  },
  medicamentItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 5,
    // borderWidth: 2,
  },
  medicamentText: {
    flex: 1,
    textAlign: 'left',
    marginHorizontal: -9,
    color: 'black',
  },
  textLight: {
    color: 'black',
  },
  separator: {
    height: 1,
    width: '100%',
  },
  separatorMargin: {
    height: 1,
    width: '85%',
    marginLeft: 24,
  },
});

export default MainPageReminderMedicaments;
