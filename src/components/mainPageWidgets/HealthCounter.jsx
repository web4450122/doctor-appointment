/* eslint-disable prettier/prettier */
// eslint-disable-next-line prettier/prettier
import React, {useState} from 'react';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import SvgMedal from '../ui/icons/SvgMedal';
import SvgCounterCalendar from '../ui/icons/SvgCounterCalendar';

const HealthCounter = ({}) => {
  return (
    <>
      <View style={styles.counterHealh}>
        <SvgMedal />
        <Text style={styles.title}>Ты молодец!{'\n'}Без приступов</Text>
        <SvgCounterCalendar />
        <View style={styles.counterItem}>
          <Text style={styles.textCounter}>2</Text>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  counterHealh: {
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 12,
    height: 55,
    width: '85%',
    borderRadius: 18,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 3,
  },
  counterItem: {
    position: 'absolute',
    height: 50,
    width: 50,
    right: 25,
  },
  title: {
    color: 'black',
    textAlign: 'center',
    fontSize: 16,
  },
  textCounter: {
    fontWeight: 'bold',
    position: 'absolute',
    textAlign: 'center',
    justifyContent: 'center',
    fontSize: 22,
    color: 'white',
    top: 9,
    left: 16,
  },
});

export default HealthCounter;
