/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, {useState} from 'react';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import Cbutton from '../ui/Cbutton';
import HorizontalSeparator from '../ui/HorizontalSeparator';
import {MAIN_COLOR} from '../../../App';

const daysWeek = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
const currentDate = new Date();
const months = [
  'Январь',
  'Февраль',
  'Март',
  'Апрель',
  'Май',
  'Июнь',
  'Июль',
  'Август',
  'Сентябрь',
  'Октябрь',
  'Ноябрь',
  'Декабрь',
];
let currentMonths = months[currentDate.getMonth()]; //
let currentYear = currentDate.getFullYear();

const getCurrentDate = () => {
  const currentDay = currentDate.getDay(); // Получаем день недели (0 - воскресенье, 1 - понедельник, и т.д.)
  const currentDateNumber = currentDate.getDate(); // Получаем текущее число
  return {currentDay, currentDateNumber};
};

const MiniCalendar = () => {
  const {currentDay, currentDateNumber} = getCurrentDate();

  return (
    <>
      <View style={styles.container}>
        <View style={styles.moTodayYearsItem}>
          <View
            style={{
              height: 30,
              width: 83,
            }}>
            <Text style={styles.text}>{currentMonths}</Text>
          </View>
          <View
            style={{
              marginLeft: '12%',
            }}>
            <Cbutton
              styleButton={styles.button}
              colorButton={{backgroundColor: MAIN_COLOR}}
              name={'Сегодня'}
              isDisabled={true}
            />
          </View>
          <View
            style={{
              marginLeft: '22%',
            }}>
            <Text style={styles.text}>{currentYear}</Text>
          </View>
        </View>
        <HorizontalSeparator
          Styles={styles.separator}
          Color={{backgroundColor: MAIN_COLOR}}
        />
        <View style={styles.calendarCubeItem}>
          {daysWeek.map((day, index) => {
            let number = '';
            let dayNumber = currentDateNumber - (currentDay - index);

            if (dayNumber <= 0) {
              const daysInPrevMonth = new Date(
                new Date().getFullYear(),
                new Date().getMonth(),
                0,
              ).getDate();
              dayNumber = daysInPrevMonth + dayNumber;
            } else {
              const daysInMonth = new Date(
                new Date().getFullYear(),
                new Date().getMonth() + 1,
                0,
              ).getDate();
              dayNumber =
                dayNumber > daysInMonth ? dayNumber - daysInMonth : dayNumber;
            }

            number = dayNumber.toString();

            return (
              <View
                key={index}
                style={[
                  styles.calendarCube,
                  index === 3
                    ? {backgroundColor: '#18D6B8'}
                    : {backgroundColor: '#D1D1D6'},
                ]}>
                <Text style={index === 3 ? {color: 'white'} : {color: 'black'}}>
                  {day}
                </Text>
                <Text style={index === 3 ? {color: 'white'} : {color: 'black'}}>
                  {number}
                </Text>
              </View>
            );
          })}
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 110,
    width: '85%',
    marginTop: 6,

    // borderWidth: 3,
  },
  moTodayYearsItem: {
    display: 'flex',
    flexDirection: 'row',
    // justifyContent: 'center',
    alignItems: 'center',
    height: 45,
    width: '100%',
    // borderWidth: 3,
    // borderColor: 'pink',
  },
  text: {
    color: 'black',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 17,
  },
  calendarCubeItem: {
    display: 'flex',
    justifyContent: 'space-around',
    flexDirection: 'row',
    height: 54,
    width: '100%',
    // borderWidth: 3,
    // borderColor: 'purple',
  },
  calendarCube: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    marginTop: 4,
    height: 40,
    width: 40,
    backgroundColor: '#D1D1D6',
    borderRadius: 8,
  },
  separator: {
    height: 2,
    width: '100%',
  },
  button: {
    height: 30,
    width: 80,
    borderRadius: 20,
  },
});

export default MiniCalendar;
