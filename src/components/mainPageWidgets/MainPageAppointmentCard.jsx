/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import HorizontalSeparator from '../ui/HorizontalSeparator';
import {MAIN_COLOR, INACTIVE_GREY} from '../../../App';
import {useSqlBuilder} from '../../hooks/useRequestBuilder';
import {appointmentFields} from '../../assets/tablesData/AppointmentFields';
import {render} from '../../assets/render/RerenderComponent';

const MainPageAppointmentCard = () => {
  const [appointmentData, setAppointmentData] = useState([]);
  const modifiedAppointmentData = appointmentData.map(item => ({
    ...item,
    doctor: singularizeDoctor(item.service),
  }));

  function singularizeDoctor(service) {
    service = service.trim();
    if (service === null || typeof service !== 'string') {
      return null;
    }
    const words = service.split(' ');
    const firstWordRemoved = words.slice(1).slice(-1).join(' ');
    const singularizedDoctor = firstWordRemoved.replace(/(?:а|я|й|ь)$/i, '');

    return singularizedDoctor;
  }

  const {openTable, updateDataSupplemented} = useSqlBuilder();
  openTable('Appointments', appointmentFields);

  useEffect(() => {
    updateDataSupplemented(
      'Appointments',
      {id: 'id', service: 'service', currentTime: 'currentTime'},
      setAppointmentData,
    );
  }, [render]);

  function checkHaveAppointmentUser() {
    if (modifiedAppointmentData.length) {
      return (
        <React.Fragment>
          {modifiedAppointmentData.map((appointment, index) => (
            <React.Fragment key={index}>
              <View style={styles.cardItem}>
                <View style={styles.cubeTime}>
                  <Text style={styles.textTime}>{appointment.currentTime}</Text>
                </View>
                <View style={{marginLeft: 10}}>
                  <Text style={styles.text}>{appointment.service}</Text>
                  <Text style={styles.text}>{appointment.doctor}</Text>
                </View>
              </View>
              {index !== modifiedAppointmentData.length - 1 && (
                <HorizontalSeparator Styles={styles.separator} />
              )}
            </React.Fragment>
          ))}
        </React.Fragment>
      );
    } else {
      return (
        <React.Fragment>
          <View style={styles.cardItem}>
            <Text style={{color: INACTIVE_GREY}}>Пусто</Text>
          </View>
        </React.Fragment>
      );
    }
  }

  return (
    <>
      <View style={styles.constainer}>
        <View style={styles.titleItem}>
          <Text style={styles.title}>ЗАПИСЬ В КЛИНИКУ</Text>
        </View>
        <HorizontalSeparator
          Styles={styles.separator}
          Color={{backgroundColor: MAIN_COLOR}}
        />
        {checkHaveAppointmentUser()}
      </View>
    </>
  );
};
//
const styles = StyleSheet.create({
  constainer: {
    display: 'flex',
    // justifyContent: 'center',
    alignItems: 'center',
    // height: 110,
    width: '85%',
    marginTop: 3,
    // borderWidth: 2,
    backgroundColor: 'white',
    borderRadius: 18,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 3,
  },
  cardItem: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
    height: 55,
    width: '85%',
    // borderWidth: 2,
    // borderColor: 'pink',
    marginTop: 2,
  },
  cubeTime: {
    display: 'flex',
    justifyContent: 'center',
    height: 30,
    width: 45,
    backgroundColor: '#18D6B8',
    borderRadius: 14,
  },
  text: {
    color: 'black',
  },
  textTime: {
    textAlign: 'center',
    color: 'white',
  },
  title: {
    marginTop: 2,
    fontSize: 19,
    color: 'black',
  },
  titleItem: {
    height: 40,
    width: '85%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-start',
    // borderWidth: 2,
    // borderColor: 'pink',
  },
  separator: {
    height: 1,
    width: '85%',
  },
  // Dseparator: {
  //   height: 1,
  //   width: '85%',
  // },
});

export default MainPageAppointmentCard;
