/* eslint-disable react/self-closing-comp */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, {useState, useEffect} from 'react';
import {MAIN_COLOR} from '../../../App';
import {SafeAreaView, StatusBar, StyleSheet, View, Text} from 'react-native';
import HorizontalSeparator from '../ui/HorizontalSeparator';
import CiconButtons from '../ui/CiconButtons';
import SvgPill from '../ui/icons/SvgNotifications/SvgPill';
import SvgAccept from '../ui/icons/SvgNotifications/SvgAccept';
import SvgCancel from '../ui/icons/SvgNotifications/SvgCancel';

const NotificationItem = () => {
  return (
    <>
      <View style={styles.root}>
        <View style={styles.headerItem}>
          <Text style={styles.headerText}>Сегодня</Text>
        </View>
        <View>
          <Text style={styles.timesOfDay}>Вечер</Text>
        </View>
        <View style={styles.cardItem}>
          <View style={styles.svgItem}>
            <View style={styles.svg}>
              <SvgPill />
            </View>
          </View>
          <View style={styles.centerItem}>
            <View style={styles.centeredContent}>
              <View style={styles.topTextItem}>
                <Text style={styles.lightText}>Препарат</Text>
                <View
                  style={[
                    styles.circleTimeIndicator,
                    {backgroundColor: MAIN_COLOR},
                  ]}>
                  <Text style={styles.timeText}>Время</Text>
                </View>
              </View>
              <HorizontalSeparator
                Styles={styles.separator}
                Color={{backgroundColor: MAIN_COLOR}}
              />
              <View style={styles.bottomTextItem}>
                <Text style={styles.lightText}>Вчера</Text>
                <Text style={styles.lightText}>100 мг</Text>
              </View>
            </View>
            <View style={styles.buttonsItem}>
              <CiconButtons icon={<SvgAccept />} onPress={() => {}} />
              <CiconButtons icon={<SvgCancel />} onPress={() => {}} />
            </View>
          </View>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  root: {
    marginTop: 20,
    height: 190,
    width: '95%',
  },
  headerItem: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 30,
    width: '100%',
    marginTop: 3,
  },
  headerText: {
    fontSize: 18,
    color: 'black',
    textAlign: 'center',
  },
  timeText: {
    color: 'white',
    textAlign: 'center',
  },
  timesOfDay: {
    fontSize: 16,
    color: 'black',
    textAlign: 'left',
  },
  lightText: {
    color: 'black',
  },
  cardItem: {
    flexDirection: 'row',
    height: 120,
    width: '100%',
    borderRadius: 15,
    backgroundColor: 'white',
    marginTop: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 3,
  },
  svgItem: {
    alignItems: 'flex-end',
    height: '100%',
    width: '15%',
  },
  svg: {
    marginTop: 10,
  },
  centerItem: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '84%',
  },
  centeredContent: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 56,
    width: '100%',
    marginBottom: 15,
  },
  topTextItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 27,
    width: '90%',
  },
  separator: {
    height: 2,
    width: '90%',
  },
  bottomTextItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 27,
    width: '90%',
  },
  circleTimeIndicator: {
    height: 20,
    width: 54,
    borderRadius: 10,
  },
  buttonsItem: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    height: 10,
    width: 120,
    marginBottom: 20,
  },
});

export default NotificationItem;
