import React, {useState} from 'react';
import {useNavigation} from '@react-navigation/native';

import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import CiconButtons from '../components/ui/CiconButtons';
import HorizontalSeparator from '../components/ui/HorizontalSeparator';
import SvgComment from './ui/icons/SvgUi/SvgComment';
import {MAIN_COLOR, DARK_GREY} from '../../App';

const MedicineCard = ({
  header,
  dose,
  visibleDoseIndicator,
  leftTopLabel,
  leftBottomLabel,
  rightTopLabel,
  rightBottomLabel,
  comment,
  description,
  onEditerButton,
  onLayout,
}) => {
  return (
    <>
      <View onLayout={onLayout} style={styles.whiteRectangle}>
        <View style={styles.headerTextItem}>
          <Text style={styles.textHeader}>{header}</Text>
          <View
            style={[
              styles.doseIndicator,
              {display: visibleDoseIndicator ? 'flex' : 'none'},
            ]}>
            <Text style={styles.textDose}>{dose}</Text>
          </View>
          <HorizontalSeparator
            Styles={styles.separator}
            Color={{backgroundColor: MAIN_COLOR}}
          />
        </View>
        <View style={styles.centerLevel}>
          <View style={styles.leftItem}>
            <View
              style={{
                height: 20,
                width: 110,
              }}>
              <Text style={[styles.textLeftTop, {color: DARK_GREY}]}>
                {leftTopLabel}
              </Text>
            </View>
            <View
              style={{
                height: 20,
                width: 125,
              }}>
              <Text style={[styles.textLeftBottom, {color: DARK_GREY}]}>
                {leftBottomLabel}
              </Text>
            </View>
          </View>

          <View style={styles.separatorItem}>
            <HorizontalSeparator Styles={styles.separatorMini} />
          </View>
          <View style={styles.rightItem}>
            <View
              style={{
                height: 20,
                width: 140,
              }}>
              <Text style={[styles.textRightTop, {color: DARK_GREY}]}>
                {rightTopLabel}
              </Text>
            </View>
            <View
              style={{
                height: 20,
                width: 140,
              }}>
              <Text style={[styles.textRightBottom, {color: DARK_GREY}]}>
                {rightBottomLabel}
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.bottomLevel}>
          <View style={styles.commentItem}>
            <View
              style={{
                height: 20,
              }}>
              <Text style={[styles.textComment, {color: DARK_GREY}]}>
                {comment}
              </Text>
            </View>
            <View
              style={{
                height: 20,
              }}>
              <Text style={[styles.textDescription, {color: DARK_GREY}]}>
                {description}
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.greenCorner}>
          <TouchableOpacity onPress={onEditerButton}>
            <Text style={styles.editButton}>...</Text>
          </TouchableOpacity>
          <View style={styles.commentButtonItem}>
            <CiconButtons icon={<SvgComment />} visibility={'none'} />
          </View>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  whiteRectangle: {
    width: '90%',
    height: 145,
    backgroundColor: 'white',
    borderRadius: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 3,
    overflow: 'hidden',
    marginTop: 20,
  },
  greenCorner: {
    position: 'absolute',
    backgroundColor: '#18D6B8',
    width: 40,
    height: '100%',
    right: 0,
    bottom: 0,
    borderTopRightRadius: 15,
  },
  headerTextItem: {
    width: '80%',
    marginLeft: 20,
    marginTop: 16,
  },
  doseIndicator: {
    backgroundColor: '#18D6B8',
    height: 20,
    width: 50,
    borderRadius: 10,
    position: 'absolute',
    top: 0,
    right: 0,
  },
  centerLevel: {
    flexDirection: 'row',
    marginTop: 5,
  },
  bottomLevel: {
    flexDirection: 'row',
    marginTop: 10,
  },
  leftItem: {
    height: 45,
    width: 110,
    // borderWidth: 1,
    // borderColor: 'green',
    marginLeft: 20,
  },
  rightItem: {
    height: 45,
    width: 140,
    // borderWidth: 1,
    // borderColor: 'green',
    marginLeft: 20,
  },
  commentItem: {
    height: 45,
    // width: 140,
    // borderWidth: 1,
    // borderColor: 'green',
    marginLeft: 20,
  },
  editButton: {
    color: 'white',
    fontSize: 30,
    fontWeight: 'bold',
    marginLeft: 8,
  },
  commentButtonItem: {
    marginTop: 60,
    marginLeft: 7,
  },
  separatorItem: {
    marginLeft: 20,
    marginTop: 3,
  },
  textHeader: {
    color: 'black',
    marginBottom: 4,
  },
  textDose: {
    textAlign: 'center',
    color: 'white',
  },
  textLeftTop: {
    fontSize: 12,
  },
  textLeftBottom: {
    fontSize: 13,
  },
  textRightTop: {
    fontSize: 12,
  },
  textRightBottom: {
    fontSize: 12,
  },
  textComment: {
    fontSize: 12,
  },
  textDescription: {
    fontSize: 13,
    color: 'black',
  },
  separator: {
    height: 2,
    width: '100%',
  },
  separatorMini: {
    height: 35,
    width: 1,
  },
});
export default MedicineCard;
