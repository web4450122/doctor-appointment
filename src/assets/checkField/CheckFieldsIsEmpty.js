/* eslint-disable prettier/prettier */
export function checkFieldsIsEmpty(fieldsToCheck) {
  fieldsToCheck.forEach(field => {
    const isEmpty = field.value.length === 0;
    field.setter(isEmpty);
  });
}
