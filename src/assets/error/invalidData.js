/* eslint-disable prettier/prettier */
import {Alert} from 'react-native';
export function invalidLoginOrPass(errorBody) {
  Alert.alert(
    'Ошибка',
    errorBody,
    [
      {
        text: 'OK',
        // onPress: () => console.log('OK Pressed'),
        style: 'cancel',
      },
    ],
    {cancelable: false},
  );
}
