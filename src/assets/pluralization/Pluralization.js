/* eslint-disable prettier/prettier */
export function getMinutesText(minutes) {
  if (minutes === 1) {
    return 'минута';
  } else if (minutes > 1 && minutes < 5) {
    return 'минуты';
  } else {
    return 'минут';
  }
}
