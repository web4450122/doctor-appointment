/* eslint-disable prettier/prettier */
export let render = false;

export function rerenderComponent() {
  render = !render;
}

// В ситуации, когда через модальное окно создается карточка, этот метод перерендерит на главной странице (MainPage) виджет и обновит его с данными о новой карточки
