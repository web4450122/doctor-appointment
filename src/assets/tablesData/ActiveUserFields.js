/* eslint-disable prettier/prettier */
export const scheme = [
  {name: 'phone', type: 'TEXT'},
  {name: 'password', type: 'TEXT'},
  {name: 'firstName', type: 'TEXT'},
  {name: 'lastName', type: 'TEXT'},
  {name: 'surName', type: 'TEXT'},
  {name: 'userId', type: 'TEXT'},
  {name: 'token', type: 'TEXT'},
  {name: 'rememberMe', type: 'INTEGER'},
];

export const fields = [
  'phone',
  'password',
  'firstName',
  'lastName',
  'surName',
  'userId',
  'token',
  'rememberMe',
];
