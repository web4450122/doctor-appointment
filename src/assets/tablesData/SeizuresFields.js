/* eslint-disable prettier/prettier */
export const seizureScheme = [
  {name: 'seizure', type: 'TEXT'},
  {name: 'currentDate', type: 'TEXT'},
  {name: 'currentTime', type: 'TEXT'},
  {name: 'continueMinutes', type: 'TEXT'},
  {name: 'sleepState', type: 'BOOLEAN'},
  {name: 'comment', type: 'TEXT'},
];

export const seizureFields = [
  'seizure',
  'currentDate',
  'currentTime',
  'continueMinutes',
  'sleepState',
  'comment',
];
