/* eslint-disable prettier/prettier */
export const appointmentScheme = [
  {name: 'service', type: 'TEXT'},
  {name: 'currentDate', type: 'TEXT'},
  {name: 'currentTime', type: 'TEXT'},
  {name: 'address', type: 'TEXT'},
  {name: 'remember', type: 'TEXT'},
];

export const appointmentFields = [
  'service',
  'currentDate',
  'currentTime',
  'address',
  'remember',
];
