/* eslint-disable prettier/prettier */
export const MedicamentsScheme = [
  {name: 'preparat', type: 'TEXT'},
  {name: 'dose', type: 'TEXT'},
  {name: 'rotationDay', type: 'TEXT'},
  {name: 'startUse', type: 'TEXT'},
  {name: 'endUse', type: 'TEXT'},
  {name: 'reminderOne', type: 'TEXT'},
  {name: 'reminderTwo', type: 'TEXT'},
  {name: 'reminderThree', type: 'TEXT'},
  {name: 'comment', type: 'TEXT'},
];

export const MedicamentsFields = [
  'preparat',
  'dose',
  'rotationDay',
  'startUse',
  'endUse',
  'reminderOne',
  'reminderTwo',
  'reminderThree',
  'comment',
];
